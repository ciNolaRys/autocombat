﻿using UnityEngine.EventSystems;

namespace Game.Controller {
  /// <summary>
  /// Override on the StandaloneInputModule to expose the PointerEventData data.
  /// </summary>
  class ContextInputModule:StandaloneInputModule {

    public PointerEventData GetLastPointerEventDataPublic(int id) {
      return GetLastPointerEventData(id);
    }
  }
}
