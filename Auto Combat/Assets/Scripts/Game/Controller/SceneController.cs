﻿using Game.Characters;
using Game.Util;
using System.Collections.Generic;
using UnityEngine;
using Game.Controller.InputContexts;

namespace Game.Controller {

  public class SceneController:Singleton<SceneController> {
    
    public static SceneController Instance {
      get {
        return _instance;
      }
    }
    //public GameObject controlPannelCanvas;

    public Dictionary<string, List<Character>> teams = new Dictionary<string, List<Character>>();
    public List<Character> AllCharacters {
      get {
        return allCharacters;
      }
    }
    private List<Character> allCharacters = new List<Character>();
    
    private Dictionary<string,List<CharacterFactory>> characters = new Dictionary<string, List<CharacterFactory>>();
    private Dictionary<string,List<Vector3>> startPositions = new Dictionary<string,List<Vector3>>();

    private string[] teamPosName = {"StartingPositionGreen","StartingPositionRed" };
    
    public InputContext Context { get { return context; } }
    public InputContext UpgradeContext { set { upgradeContext = value; } }
    private InputContext context;
    private InputContext upgradeContext;
    
    void Start() {
      Cursor.lockState = CursorLockMode.Confined;
      
      // loads all object under the scene object.
      Transform[] t = GetComponentsInChildren<Transform>();
      for (int i = 0;i < t.Length;i++) {
        if (t[i].name.StartsWith("StartingPosition"))
          SetupStartPosition(t[i].gameObject);
      } 
    }
    
    /// <summary>
    /// 
    /// </summary>
    void Update() {
      ApplyContext();
    }
    
    /// <summary>
    /// Apply a context. 
    /// When the returned context is null, this usually means no context has been found.
    /// Its up to the context implementation to setup how if it get kept, upgraded or removed.
    /// Removing a context is done by sending back the parentContext.
    /// Keeping a context is done by sending back itself.
    /// upgrading a context is done by setting the SceneController.setContext()
    ///   Set upgradeContext with the new context you want to upgrade.
    /// Upgrading will replace the context when found, or add it when not found.
    /// </summary>
    private void ApplyContext() {
      InputContext newContext = context.ApplyContext();
      if ( newContext != null )
        context = newContext;
      if (upgradeContext != null) {
        SetContext(upgradeContext);
      }
    }
    
    public void ResetContext() {
      if ( context != null ) { 
        context.ResetContext();
        context = null;
      }
    }

    public InputContext SetContext(InputContext ctxt) {

      if ( context == null ) {
        context = ctxt;
      }

      InputContext newContext = null;
      newContext = context.Upgrade(ctxt);

      if (newContext == null) { 
        newContext = ctxt;
        newContext.parentContext = context;
        context = newContext;
      } else
        context = newContext;

      upgradeContext = null;
      return context;
    }
    
    public InputContext ConsumeContext(string contextString) {
      context = context.Consume(contextString);
      return context;
    }

    public InputContext ConsumeContext(InputContext contxt) {
      context = context.Consume(contxt);
      return null;
    }
    
    private void StartBattle() {
      int teamIdx = 0;
      foreach (KeyValuePair<string,List<CharacterFactory>> c in characters) {
        LoadCharacterIntoScene(c.Value,teamPosName[teamIdx]);
        teamIdx++;
      }
    }

    /// <summary>
    /// Happens when a player hit apply on the character selection screen.
    /// </summary>
    /// <param name="c"></param>
    /// <param name="team"></param>
    public void LockSelection(List<CharacterFactory> c,string team) {
      characters[team] = c;
    }

    /// <summary>
    /// keep in memory all the positions
    /// </summary>
    /// <param name="startPositionGroup"></param>
    private void SetupStartPosition(GameObject startPositionGroup) {
      Transform[] t = startPositionGroup.GetComponentsInChildren<Transform>();
      startPositions[startPositionGroup.name] = new List<Vector3>();
      for (int i = 0;i < t.Length;i++) {
        startPositions[startPositionGroup.name].Add(t[i].position);
      }
    }

    /// <summary>
    /// Loads Character into one position for its team.
    /// </summary>
    /// <param name="characterList"></param>
    /// <param name="teamPosName"></param>
    private void LoadCharacterIntoScene(List<CharacterFactory> characterList,string teamPosName) {
      List<Vector3> pos = startPositions[teamPosName];
      for (int i = 0;i < characterList.Count;i++) {
        Instantiate(characterList[i].PrefabObject,pos[i],new Quaternion());
      }
    }

    /// <summary>
    /// Adds a governer into a team.
    /// </summary>
    /// <param name="governer">the character to add.</param>
    /// <param name="team">the team to add the character into.</param>
    public void RegisterTeamMember(Character character,string team) {
      if (!teams.ContainsKey(team))
        teams.Add(team,new List<Character>());
      teams[team].Add(character);
      allCharacters.Add(character);
    }

    /// <summary>
    /// gets the size of the requested team.
    /// </summary>
    /// <param name="team">Team which we need the size of.</param>
    /// <returns></returns>
    public int GetTeamSize(string team) {
      List<Characters.Character> characters;

      if (teams.TryGetValue(team,out characters)) {
        return characters.Count;
      }
      return 0;
    }

    /// <summary>
    /// Deregister a Character from the SceneCharacter List
    /// </summary>
    /// <param name="governer"></param>
    /// <param name="team"></param>
    public void RemoveTeamMember(Character character,string team) {
      teams[team].Remove(character);
    }


  }
}