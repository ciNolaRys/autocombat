﻿using Game.Controller.InputContexts;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Game.Controller {
  public interface ISlotController {
    /// <summary>
    /// Creates an UI object that the user can control with the mouse.
    /// </summary>
    /// <param name="data"></param>
    /// <param name="containerSlot"></param>
    /// <returns></returns>
    InputContextDragging CreateDraggingObject(PointerEventData data);

    /// <summary>
    /// Resets a slot, removing any child GameObject and reseting its properties.
    /// </summary>
    void ResetSlot();

    
  }
}
