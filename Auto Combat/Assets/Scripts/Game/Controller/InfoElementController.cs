﻿using Game.Spells;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Controller {
  class InfoElementController:MonoBehaviour {

    public Image icon;
    public Text text;

    private RectTransform iconRect;
    private Vector2 iconSize;
    
    /// <summary>
    /// Sets the content of one spell in the UI.
    /// </summary>
    /// <param name="spellSprite">the sprite for that spell</param>
    /// <param name="spellText">the text for this apell</param>
    public void SetContent(string text, Sprite sprite = null) {
      icon.sprite = sprite;
      this.text.text = text;

      if ( icon.sprite == null )
        iconRect.sizeDelta = Vector2.zero;
      else
        iconRect.sizeDelta = iconSize;

    }

    /// <summary>
    /// Removes the content for this spell.
    /// </summary>
    public void RemoveContent() {
      icon.sprite = null;
      icon.color = Util.Utils.ConvertColor(0, 0, 0, 0);
      text.text = "";
    }


    void Start() {
      iconRect = icon.gameObject.GetComponent<RectTransform>();
      iconSize = iconRect.rect.size;
    }
    
  }
}
 