﻿
using Game.Util;
using UnityEngine;

namespace Game.Controller{
  /// <summary>
  /// This Interface must be implemented on Object that will be on a Inventory Slot.
  /// </summary>
  public interface IContentInstance {

    EnumPool SlotPool { get; }
    string Description { get; }
    /// <summary>
    /// A butiful name that could be displayed on a UI element.
    /// </summary>
    string Name { get; }

    Sprite Sprite { get; }
    /// <summary>
    /// 
    /// </summary>
    GameObject PrefabObject { get; set; }
  
  }
}
