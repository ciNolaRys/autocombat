﻿namespace Game.Controller.InputContexts {
  public enum Context {
    CharacterSelected,
    Dragging,
    ItemSelect,
    Main,
    MainMenu,
    SceneMenu,
    SquadMenu

  }
}
