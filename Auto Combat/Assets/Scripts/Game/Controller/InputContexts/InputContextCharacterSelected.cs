﻿using Game.Characters;
using Game.Controller.PanelControllers;
using Game.InventorySystem;
using Game.UI;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Controller.InputContexts
{
  /// <summary>
  /// This context is on when a character is selected in an active scene.
  /// This context holds information about the selected character and is the link between
  /// the character and the control panel.
  /// </summary>
  public class InputContextCharacterSelected : InputContext
  {

    /// <summary>
    /// The Character that is currently selected.
    /// </summary>
    private Character selectedCharacter;

    /// <summary>
    /// Do not use! throws an exception.
    /// </summary>
    public InputContextCharacterSelected() { }

    /// <summary>
    /// Use with SceneController.setContext only
    /// </summary>
    /// <param name="character"></param>
    public InputContextCharacterSelected(Character character)
    {
      Init(character);
    }

    /// <summary>
    /// Use only when inserting the context inside the tree.
    /// </summary>
    /// <param name="character"></param>
    /// <param name="ctxt"></param>
    public InputContextCharacterSelected(Character character, InputContext ctxt)
    {
      parentContext = ctxt;
      Init(character);
      selectedCharacter = character;
      executor = character;
      selectedCharacter.SelectObject.SetActive(true);
    }

    /// <summary>
    /// Common Initializer. This should be call from each valid constructor.
    /// </summary>
    /// <param name="character"></param>
    private void Init(Character character)
    {
      Init();
      contextString = "Selection";
      controlPannelController = parentContext.ControlPannelController;
      controlPannelController.SelectCharacter(character);

    }

    /// <summary>
    /// Click on a UI element and act depending on what has been clicked.
    /// This is when the user click on an item on the control Panel.
    /// </summary>
    /// <returns></returns>
    public override InputContext LeftClickUI()
    {
      Debug.Log("Mouse Left Click UI() on character Selected.");
      InputContext newContext = this;
      SlotController slotController = null;
      PanelController parentPanel = null;
      GameObject clickedSlot = data.pointerEnter.transform.gameObject;

      slotController = clickedSlot.GetComponent<SlotController>();

      if (slotController != null && !slotController.IsEmpty)
      {
        parentPanel = slotController.PanelCtrl;
        newContext = CreateNewContext(slotController.CreateDraggingObject(data), this);
        if (parentPanel.isSwappable) { 
          slotController.ResetSlot(); 
        }
      }

      return newContext;
    }

    /// <summary>
    /// deselects the current selection and call the parent to see if a new selection has been made.
    /// </summary>
    /// <returns></returns>
    public override InputContext LeftClick()
    {
      InputContext newContext = this;
      selectedCharacter.SelectObject.SetActive(false);
      controlPannelController.SelectCharacter(null);
      newContext = parentContext.ApplyContext();
      SceneController.Instance.UpgradeContext = newContext;
      return newContext;
    }

    /// <summary>
    /// RightClick on a floor will trigger movement for the selected character.
    /// </summary>
    /// <returns></returns>
    public override InputContext RightClick()
    {
      InputContext newContext = this;
      // TODO right click could have a second function, Interract with an interactable object.
      RaycastHit hit;
      Ray camRay = Camera.main.ScreenPointToRay(Input.mousePosition);
      int layerMask = (1 << LayerMask.NameToLayer("floor"));
      if (Physics.Raycast(camRay, out hit, 1000, layerMask))
      {
        selectedCharacter.MoveController.SetDestination(hit.point);
      }
      return newContext;
    }

    /// <summary>
    /// Specific ToString function for chartacterSelected();
    /// </summary>
    /// <returns></returns>
    public override string ToString()
    {

      string ctxt = base.ToString();
      string name = "";
      if (selectedCharacter != null)
        name = selectedCharacter.name;

      return ctxt + "-" + name;
    }

  }
}