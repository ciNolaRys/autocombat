﻿using Game.Controller.PanelControllers;
using Game.Util;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Game.Controller.InputContexts {

  /// <summary>
  /// This context exists when an Item gets selected in the squad select screen.
  /// </summary>
  public class InputContextItemSelect : InputContext {

    private SlotController currentSlotController;
    private PanelControllerSquad currentPanelController;
    private PanelControllerInfo details;
    

    // TODO Set the characterDetailPanelController.
    public InputContextItemSelect(SlotController slotController, InputContext context) : base (context)  {
      Init(slotController);
      
    }

    public InputContextItemSelect(SlotController slotController) {
      Init(slotController);
      
    }
    
    public void Init(SlotController slotController) {
      currentSlotController = slotController;
      currentPanelController = slotController.gameObject.GetComponentInParent<PanelControllerSquad>();
      details = slotController.GetComponentInParent<PanelControllerInfo>();
      base.Init();
      contextString = "ItemSelected";
    }
    
    public override InputContext RightClickUI() {
      if ( isGUIContext ) {
        return this;
      }
      return Consume(this);
    }

    public override InputContext Upgrade(InputContext c) {

      if ( c is InputContextItemSelect )
        currentSlotController.UnselectItem();
      return base.Upgrade(c);
    }

    public override InputContext Consume(InputContext ctxt) {
      InputContext newContext = parentContext;
      currentSlotController.UnselectItem();
      newContext = base.Consume(ctxt);
      return newContext;
    }
    
    public override InputContext LeftClickUI() {
      
      InputContext newContext = this;
      GameObject clickedItem = data.pointerEnter.transform.gameObject;
      SlotController clickedSlotController = clickedItem.GetComponentInParent<SlotController>();
      PanelControllerSquad clickedPanelController = null;
      SlotController tempSlotController = null;

      if ( clickedSlotController == null )
        return newContext;

      // Clicking on the same item will deselect it.
      if ( clickedSlotController.ItemObject == currentSlotController.ItemObject) {
        return Consume(this);
      }
      
      clickedPanelController = (PanelControllerSquad)clickedSlotController.PanelCtrl;

      if ( currentPanelController.isSwappable ) {
        //TODO : put logic for swaping / replacing object.
        // this should probably be in the Input Context Dragging class.

        GameObject tempSlot = Pooler.Instance.GetPooledObject(currentSlotController.PoolType);         
        tempSlotController = tempSlot.GetComponent<SlotController>();
        tempSlotController.SetContent(clickedSlotController);

        clickedSlotController.ResetSlot();
        clickedSlotController.CreateItem(currentSlotController);

        currentSlotController.ResetSlot();
        currentSlotController.CreateItem(tempSlotController);
        
        Pooler.Instance.RecycleObjectAndChildren(tempSlot);
        currentSlotController.UnselectItem();
        clickedSlotController.UnselectItem();
        

      } else {
        currentPanelController.ResetHighlight();
        if ( currentPanelController.Equals(clickedPanelController) ) {
          newContext = clickedSlotController.SelectItem();
          return newContext;
        } else if ( clickedPanelController.isSwappable ) {
          clickedSlotController.ResetSlot();
          clickedSlotController.CreateItem(currentSlotController);
        }
      }
      
      return Consume(this);
    }

    public override InputContext LeftClick() {     
      return base.LeftClick();
    }
  }
}
