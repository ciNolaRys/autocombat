﻿using Game.Controller.PanelControllers;
using Game.Util;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Game.Controller.InputContexts {
  /// <summary>
  /// Controls the Input when an object is being dragged by a user.
  /// Drag and drop is handled about the same way as the click and drop.
  /// </summary>
  public class InputContextDragging : InputContext {

    /// <summary>
    /// Container or visual of the DraggedObject.
    /// </summary>
    public GameObject DraggedObject {
      get { return draggedObject; }
      set {
        draggedObject = value;
        draggedObject.GetComponent<CanvasGroup> ().blocksRaycasts = false;
      }
    }
    /// <summary>
    /// Type or Class (or Brains) of the DraggedObject.
    /// </summary>
    public IContentInstance DraggedObjectType {
      get { return draggedObjectType; }
    }

    public Vector2 Offset { get { return offset; } }
    public Transform DraggingCanvas { get { return draggingCanvas; } }
    private IContentInstance draggedObjectType;
    private Vector2 offset;

    /// <summary>
    /// The GameObject that is being Dragged.
    /// </summary>
    private GameObject draggedObject = null;

    /// <summary>
    /// A canvas where we can draw the dragged UI element.
    /// </summary>
    private Transform draggingCanvas;
    /// <summary>
    /// The instance of the holding panel script.
    /// </summary>
    private PanelController panelCtrl;

    public InputContextDragging () {
      Init ();
    }

    /// <summary>
    /// Contructor
    /// </summary>
    /// <param name="classInstance">Instance of the class of the dragged item</param>
    /// <param name="label"></param>
    /// <param name="offset">distance between the center of the object and the mouse pointer</param>
    /// <param name="controller">The instance of the script on the holding pannel (ex.: behaviorController)</param>
    public InputContextDragging (SlotController originSlot, Vector2? offset) {

      Init ();
      isDragging = false;
      this.offset = offset ?? new Vector2 (0f, 0f);

      panelCtrl = originSlot.PanelCtrl;
      draggedObject = originSlot.CreateItem (originSlot);
      draggedObject.transform.SetParent (draggingCanvas, true);
      draggedObjectType = originSlot.ClassInstance;

    }

    protected override void Init () {
      draggingCanvas = GameObject.FindGameObjectWithTag ("DraggingItem").transform;
      base.Init ();
      contextString = "Dragging";
    }

    public override InputContext ApplyOverGUI () {
      data = EventSystem.current.gameObject.GetComponent<ContextInputModule> ().GetLastPointerEventDataPublic (-1);

      if (data.dragging && !isDragging) {
        isDragging = data.dragging;
      }

      // triggers only when dragging ends.
      if (Input.GetButtonUp ("MouseLeftClick") /*&& ((isDragging && !data.dragging)) */ ) {
        Debug.Log ("leftclick on button up while dragging.");
        return base.ApplyContext (InputFunction.LeftClickUI);
      }

      if (Input.GetButtonUp ("MouseRightClick"))
        return base.ApplyContext (InputFunction.RightClickUI);

      // // button down will trigger only when the object is in click and drop mode.
      // if (Input.GetButtonDown ("MouseLeftClick")) {
      //   Debug.Log ("MouseLeftClick on button down while dragging.");
      //   return base.ApplyContext (InputFunction.LeftClickUI);
      // }

      return this;
    }

    public void FollowMouse () {
      DraggedObject.transform.position = (Vector2) Input.mousePosition - offset;
    }

    public void DropObject (PointerEventData data) {

      Transform newSlotTransform = null;
      GameObject receivingObject = data.pointerEnter.transform.gameObject;
      SlotController receivingController = receivingObject.GetComponent<SlotController> ();

      if (receivingController != null && receivingController.PanelCtrl.isSwappable) {
        newSlotTransform = receivingObject.transform;
        // Destroy the existing Item when it exists. There's always only one item / slot.
        if (newSlotTransform.childCount > 0) {
          Pooler.Instance.RecycleChildren (newSlotTransform.gameObject);
        }
        GameObject slot = receivingController.CreateItem (draggedObjectType);
        receivingController.PanelCtrl.UpdateSlots ();
      }

    }

    /// <summary>
    /// Destroy the dragging Object and returns the parent (removing the context).
    /// </summary>
    /// <param name="ctxt"></param>
    /// <returns></returns>
    public override InputContext Consume (InputContext ctxt) {
      InputContext newContext = null;
      newContext = base.Consume (ctxt);

      if (ctxt.Equals (this)) {
        //panelCtrl.UpdateSlots();
        Pooler.Instance.RecycleChildren (DraggingCanvas.gameObject);
      }

      return newContext;
    }

    public override InputContext LeftClick () {
      //if ( draggedObjectOrigin != null ) { 
      //  draggedObjectOrigin.transform.parent = null;
      //  Object.Destroy(draggedObjectOrigin);
      //}
      return Consume (this);
    }
    public override InputContext LeftClickUI () {
      DropObject (data);
      return Consume (this);
    }
    public override InputContext RightClick () {
      return Consume (this);
    }
    public override InputContext RightClickUI () {
      return Consume (this);
    }

    public override InputContext Cancel () {
      return Consume (this);
    }

  }
}