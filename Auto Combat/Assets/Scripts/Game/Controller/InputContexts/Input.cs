﻿

namespace Game.Controller.InputContexts {
  /// <summary>
  /// List of all possible Function and Key used in the game.
  /// </summary>
  public enum InputFunction {

    LeftClick ,
    LeftClickUI,
    RightClick ,
    RightClickUI,
    Cancel ,
    
  } 
}