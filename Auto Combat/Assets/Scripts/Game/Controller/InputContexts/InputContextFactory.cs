﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Controller.InputContexts {
  public class InputContextFactory {

   
    public InputContext getInputContext(Context context) {

      switch ( context ) {
        case Context.CharacterSelected:
          return new InputContextCharacterSelected();
        case Context.Dragging:
          return new InputContextDragging(); 

      }

      return null;
    }

    //private InputContextCharacterSelected FactoryCharacterSelected( object inputObject ) {

    //  if ( inputObject.GetType()== typeof(InputContext) ) {
    //    return new InputContextCharacterSelected( (InputContext) inputObject );
    //  }

    //  return null;

    //}
     
  }
}