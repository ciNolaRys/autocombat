﻿using Game.Characters;
using Game.UI;
using UnityEngine;

namespace Game.Controller.InputContexts {
  /// <summary>
  /// Main Context for a Scene. This should always be instanciated at the start of the Scene.
  /// </summary>
  public class InputContextMain:InputContext  {
    

    public InputContextMain() {
      
      Init();
    }

    protected override void Init() {
      
      //GameObject controlPannel = (GameObject) Object.Instantiate(Resources.Load("prefabs/UI/ControlPanel/ControlCanvas", typeof(GameObject)));
      //controlPannelController = controlPannel.GetComponent<ControlPanelController>();

      controlPannelController = GameObject.FindObjectOfType<ControlPanelController>();
      base.Init();
      contextString = "Main";
    }

    public override InputContext LeftClick() {
      InputContext newContext = this;
      RaycastHit hit;
      Ray camRay = Camera.main.ScreenPointToRay(Input.mousePosition);
      int layerMask = (1 << LayerMask.NameToLayer("character")) ;
      Character selectedCharacter;
      
      if ( Physics.Raycast(camRay,out hit,1000,layerMask) ) {
        selectedCharacter = hit.collider.transform.gameObject.GetComponent<Character>();
        newContext = new InputContextCharacterSelected(selectedCharacter,this);
        SceneController.Instance.UpgradeContext = newContext;
      }
      
      return newContext;
    }

    public override InputContext Cancel() {
      //gameController.OpenSceneMenu();
      return this;
    }

    public override InputContext Consume(InputContext ctxt) {      
      return this;
    }

  }
}