﻿using Game.Controller.PanelControllers;
using Game.Users;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Game.Controller.InputContexts {
  class InputContextSquadMenu : InputContext{

    private GameObject squadMenuCanvas;
    private SquadCanvasController canvasController;
    
    /// <summary>
    /// Constructor. sets the GameObject and Controller values.
    /// </summary>
    /// <param name="user"></param>
    /// <param name="squadMenuCanvas"></param>
    public InputContextSquadMenu(User user) {
      contextString = "SquadMenu";
      squadMenuCanvas = GameController.Instance.squadMenuCanvas;
      squadMenuCanvas.SetActive(true);
      canvasController = squadMenuCanvas.GetComponent<SquadCanvasController>();
      canvasController.User = user;
      canvasController.Init();
      Init();
    }
    
    public override InputContext ApplyOverGUI() {
      data = EventSystem.current.gameObject.GetComponent<ContextInputModule>().GetLastPointerEventDataPublic(-1);
      if ( Input.GetButtonDown("MouseLeftClick") )
        return ApplyContext(InputFunction.LeftClickUI);

      if ( Input.GetButtonUp("MouseRightClick") )
        return ApplyContext(InputFunction.RightClickUI);

      return this;
    }
    
    public override InputContext Consume(InputContext ctxt) {
      InputContext newContext = base.Consume(ctxt);
      if ( ctxt.Equals(this) ) {
        squadMenuCanvas.SetActive(false);
      }
      return newContext;
    }

    public override InputContext Consume(string ctxt) {
      InputContext newContext = base.Consume(ctxt);
      if ( ctxt.Equals(this.contextString) ) {
        squadMenuCanvas.SetActive(false);
      }
      return newContext;
    }
    
    public override InputContext RightClickUI() {

      SlotController clickedController = data.pointerEnter.gameObject.GetComponentInParent<SlotController>();

      if ( clickedController != null  && clickedController.PanelCtrl.isItemDestroyable) {
        clickedController.ResetSlot();
      }
      
      return this;

    }

    public override InputContext LeftClickUI() {

      InputContext newContext = this;
      GameObject clickedObj = data.pointerEnter.transform.gameObject;
      SlotController newSlot = clickedObj.GetComponentInParent<SlotController>();

      if ( newSlot == null )
        return newContext;

      PanelControllerSquad newController = (PanelControllerSquad) newSlot.PanelCtrl;

      if ( !newSlot.IsEmpty ) {
        newController.ResetHighlight();
        return newSlot.SelectItem();
      }
      
      return newContext;
    }


  }
}
