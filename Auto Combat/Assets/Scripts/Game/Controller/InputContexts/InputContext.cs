﻿using System;
using Game.Characters;
using Game.Spells;
using Game.UI;
using Game.Util;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Game.Controller.InputContexts {
  public abstract class InputContext {

    public SpellFactory currentAbility;
    public InputContext parentContext;
    public Character Executor {
      get { return executor; }
    }
    public CameraControl camCtrl;
    public ControlPanelController ControlPannelController { get { return controlPannelController; } }

    protected string contextString = "InputContext - This can't be initialized.";
    protected Character executor;
    protected PointerEventData data;
    protected ControlPanelController controlPannelController;
    protected bool isGUIContext = false;
    protected bool isDragging;

    //Constructors
    public InputContext () { }
    public InputContext (InputContext parentContxt) {
      parentContext = parentContxt;
    }

    /// <summary>
    /// Basic Initializer from the Abstract InputContext class.
    /// </summary>
    protected virtual void Init () {
      camCtrl = Camera.main.gameObject.GetComponent<CameraControl> ();

    }

    /// <summary>
    /// Called each Update(), checks for an input and calls the corresponding function.
    /// </summary>
    /// <returns></returns>
    public InputContext ApplyContext () {

      if (Input.GetButtonUp ("Cancel")) {
        return ApplyContext (InputFunction.Cancel);
      }

      if (IsOverGUI ()) {
        return ApplyOverGUI ();
      }

      if (Input.GetButtonUp ("MouseLeftClick")) {
        if (isGUIContext) {
          isGUIContext = false;
          return (this);
        }
        return ApplyContext (InputFunction.LeftClick);
      }

      if (Input.GetButtonUp ("MouseRightClick")) {
        return ApplyContext (InputFunction.RightClick);
      }

      if (Input.GetAxis ("Mouse ScrollWheel") > 0)
        camCtrl.ZoomForward ();

      if (Input.GetAxis ("Mouse ScrollWheel") < 0)
        camCtrl.ZoomBackward ();

      if (Input.GetButton ("MouseMiddleClick"))
        camCtrl.CameraTranslation ();

      if (parentContext != null && 1 == 2)
        return parentContext.ApplyContext ();

      return this;
    }

    /// <summary>
    /// Basic GUI processing
    /// </summary>
    /// <returns></returns>
    public virtual InputContext ApplyOverGUI () {
      data = EventSystem.current.gameObject.GetComponent<ContextInputModule> ().GetLastPointerEventDataPublic (-1);
      if (data.dragging && !isDragging) {
        isDragging = data.dragging;
      }

      if (isDragging) {
        isGUIContext = true;
        if (Input.GetButtonUp ("MouseLeftClick")) {
          Debug.Log ("InputContext Button UP Left (WHILE DRAGGING)");
          isGUIContext = false;
          isDragging = false;
        }
        return this;
      }

      if (Input.GetButtonUp ("MouseLeftClick")) {
        Debug.Log ("InputContext Button UP Left (NOT DRAGGING)");
        return ApplyContext (InputFunction.LeftClickUI);
      }

      if (Input.GetButtonUp ("MouseRightClick")) {
        return ApplyContext (InputFunction.RightClickUI);
      }

      return this;
    }

    /// <summary>
    /// is this required really? 
    /// </summary>
    /// <param name="data"></param>
    /// <returns></returns>
    protected GameObject GetUIElement (PointerEventData data) {

      return data.pointerEnter.gameObject;

    }

    /// <summary>
    /// Recursive fonction that apply the input fired. Returns a new InputContext upgraded.
    /// </summary>
    /// <param name="button"></param>
    /// <returns></returns>
    protected virtual InputContext ApplyContext (InputFunction button) {
      InputContext newContext = this;

      newContext = (InputContext) typeof (InputContext).GetMethod (Enum.GetName (typeof (InputFunction), button)).Invoke (this, null);
      if (newContext == null) {
        newContext = parentContext.ApplyContext (button);
      }

      return newContext;
    }

    /// <summary>
    /// Recursively Set the context from topmost parentContext.
    /// </summary>
    /// <param name="c"></param>
    /// <returns>the resulting context of this upgrade.</returns>
    public virtual InputContext Upgrade (InputContext c) {

      InputContext newContext = null;

      // go get the topmost parent.
      if (parentContext != null)
        newContext = parentContext.Upgrade (c);

      // If there is newContext, that means the parent was changed.
      if (newContext != null) {
        parentContext = newContext;
        return this;
      }

      // Check the condition to change a context. When true return the object.
      if (contextString == c.contextString) {
        newContext = c;
        newContext.parentContext = parentContext;
        return newContext;
      }
      return newContext;
    }

    public virtual InputContext CreateNewContext (InputContext newContext, InputContext parent) {

      newContext.currentAbility = parent.currentAbility;
      newContext.parentContext = parent;
      newContext.executor = parent.executor;

      return newContext;
    }

    /// <summary>
    /// Return the context object you want to be set at that context level.
    /// For example, returning the parent context removes the current context.
    /// Returning the same context will keep the context intact.    
    /// To consume a context AND its parent, call the parentContext.Apply().
    ///   The parent should return its parent and then, the current will return 
    ///   the context the parent returned, effectively removing 2 context from the tree.
    /// call SceneController.Instance.upgradeContext in the implementation to upgrade the context.
    ///   This can be done in addition to removing parent contexts. 
    ///   
    /// This usually take the for of:
    ///       InputContext newContext = this;
    ///       *put your code here*
    ///       return newContext;
    /// </summary>
    /// <returns>This context, of the parent if applying the context.</returns>
    public virtual InputContext LeftClick () {
      return this;
    }
    public virtual InputContext LeftClickUI () {
      return this;
    }
    public virtual InputContext RightClick () {
      return this;
    }
    public virtual InputContext RightClickUI () {
      return this;
    }
    public virtual InputContext Cancel () {
      return this;
    }

    /// <summary>
    /// String representation of the context;
    /// </summary>
    /// <returns></returns>
    public override string ToString () {

      while (parentContext != null) {
        return parentContext.ToString () + "\n" + contextString;
      }
      return contextString;
    }

    /// <summary>
    /// Determine if the cursor is over a GUI element on the screen.
    /// </summary>
    /// <returns>whether the cursor is over a GUI element.</returns>
    public bool IsOverGUI () {
      return EventSystem.current.IsPointerOverGameObject ();
    }

    public bool MouseMiddleClick () {
      return Input.GetButton ("MouseMiddleClick");
    }

    /// <summary>
    /// Recurcive function that removes the passed context.
    /// When Overriding this, the first 2 lines should always be :
    /// InputContext newContext = null;
    /// newContext = base.Consume(ctxt);
    /// And shoud return newContext.
    /// </summary>
    /// <param name="ctxt">Context to be removed.</param>
    /// <returns>the new context without minus the removed context.</returns>
    public virtual InputContext Consume (InputContext ctxt) {

      if (parentContext != null)
        parentContext.Consume (ctxt);

      if (ctxt.contextString.Equals (contextString))
        return parentContext;

      return this;

    }

    public virtual InputContext Consume (string ctxtStr) {

      if (parentContext != null)
        parentContext.Consume (ctxtStr);

      if (ctxtStr.Equals (contextString))
        return parentContext;

      return this;

    }

    /// <summary>
    /// Consumes all contexts in order.
    /// </summary>
    public virtual void ResetContext () {

      InputContext ctxt = Consume (this);
      if (ctxt != null && ctxt.contextString != "Main")
        ctxt.ResetContext ();

    }

    /// <summary>
    /// I don't this this is useful after all.
    /// It's an attempt of lazy return the context if it's of the correct type
    /// But since it's returning only the InputContext as the out, it's not useful.
    /// </summary>
    /// <param name="type"></param>
    /// <param name="outContext"></param>
    /// <returns></returns>
    public virtual bool IsContextType (Type type, out InputContext outContext) {
      outContext = null;
      if (GetType () == type) {
        outContext = this;
        return true;
      }
      return false;
    }

  }
}