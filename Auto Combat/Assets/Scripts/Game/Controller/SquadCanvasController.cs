﻿using Game.Controller.PanelControllers;
using Game.Users;
using UnityEngine;

namespace Game.Controller {
  public class SquadCanvasController:MonoBehaviour {

    public  User User { get; set; }
    

    private PanelController selectedCharacterPanel;
    private PanelController availableChracterPanel;
    private PanelController characterDetailsPanel;
    
    public void Init() {

      selectedCharacterPanel = GetComponentInChildren<PanelControllerSelectedCharacter>();
      availableChracterPanel = GetComponentInChildren<PannelControllerAvailableCharacter>();
      characterDetailsPanel  = GetComponentInChildren<PanelControllerInfo>();

      selectedCharacterPanel.Init();
      availableChracterPanel.Init();
      characterDetailsPanel.Init();
    }

    public void CloseSquadMenu() {
      availableChracterPanel.ClearUI();
      selectedCharacterPanel.ClearUI();
      characterDetailsPanel.ClearUI();
      SceneController.Instance.ConsumeContext("SquadMenu");
    }

  }
}
