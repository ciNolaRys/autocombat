﻿using Game.Controller.InputContexts;
using Game.Controller.PanelControllers;
using Game.Util;
using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Game.Controller {
  /// <summary>
  /// All Inventory Slots must have this object attached. Right now, only the Behavior Inventory System is using this.
  /// </summary>
  public class SlotController : MonoBehaviour {

    // Editor Fields

    // Properties
    /// <summary>
    /// Check if a class instance is set on the slotController.
    /// </summary>
    public bool IsEmpty { get { return classInstance == null; } }
    /// <summary>
    /// holds the class associated with the gameObject. (ex.: Ability, Condition, etc ...)
    /// </summary>    
    public IContentInstance ClassInstance { get { return classInstance; } }
    /// <summary>
    /// The pannelController.
    /// </summary>
    public PanelController PanelCtrl { get; set; }
    /// <summary>
    /// The Item object this Slot contains.
    /// </summary>
    public GameObject ItemObject { get; private set; }

    public EnumPool PoolType { get { return pool.poolType; } set { pool.poolType = value; } }

    public bool ShowLabel { get; set; }
    public bool ShowImage { get; set; }

    public Type RequiredType { get { return pool.RequiredType; } }
    // Private fields
    /// <summary>
    /// The HighLight game object attached to the child.
    /// </summary>
    private GameObject highlightObj;
    /// <summary>
    /// pointer to the Text componant.
    /// </summary>
    private Text text;
    /// <summary>
    /// pointer to he Image component
    /// </summary>
    private Image image;
    /// <summary>
    /// The label for that particular slot.
    /// </summary>
    private string label;
    /// <summary>
    /// The sprite to show in the image.Sprite
    /// </summary>
    private Sprite sprite;

    /// <summary>
    /// holds the class associated with the gameObject.
    /// </summary>
    private IContentInstance classInstance;
    /// <summary>
    /// Define what type of object this slot accepts.
    /// </summary>

    //private Type requiredType = null;
    //private EnumPool poolType;
    private Pool pool;
    // Methods 
    
    void Awake() { pool = GetComponent<Pool>(); }
    void Start() {
      PanelCtrl = GetComponentInParent<PanelController>();

    }

    void OnDisable() {
      highlightObj = null;
      image = null;
      text = null;
      sprite = null;
      label = "";
      pool.poolType = EnumPool.None;
      classInstance = null;
      PanelCtrl = null;
      ItemObject = null;
    }

    public void Init() {

      if ( ItemObject == null )
        return;

      Transform highlightTransform = Utils.findOneGameObjectInChildrenByName("Highlight", ItemObject);
      Transform imageTransform = Utils.findOneGameObjectInChildrenByName("Image", ItemObject);
      Transform textTransform = Utils.findOneGameObjectInChildrenByName("Text", ItemObject);

      if ( highlightTransform != null ) {
        highlightObj = highlightTransform.gameObject;
        UnselectItem();
      }

      if ( imageTransform != null ) {
        image = imageTransform.gameObject.GetComponent<Image>();
        image.sprite = null;
        if ( !ShowImage )
          image.gameObject.SetActive(false);
        else
          image.gameObject.SetActive(true);
      }

      if ( highlightTransform != null ) {
        text = textTransform.gameObject.GetComponent<Text>();
        text.text = "";
        if ( !ShowLabel )
          text.gameObject.SetActive(false);
        else
          text.gameObject.SetActive(true);
      }

    }

    /// <summary>
    /// Sets the Highlight object Active.
    /// </summary>
    /// <returns></returns>
    public InputContextItemSelect SelectItem() {
      if ( highlightObj == null )
        Init();
      InputContextItemSelect newcontext = new InputContextItemSelect(this);
      highlightObj.GetComponent<Image>().enabled = true;
      SceneController.Instance.SetContext(newcontext);
      return newcontext;
    }

    /// <summary>
    /// Disable the highlight.
    /// </summary>
    public void UnselectItem() {
      highlightObj.GetComponent<Image>().enabled = false;
    }
    
    /// <summary>
    /// Set the content of this slot, based on another slot.
    /// </summary>
    /// <param name="slotCtrl"></param>
    public bool SetContent(SlotController slotCtrl) {

      return SetContent(slotCtrl.classInstance, slotCtrl.ItemObject, slotCtrl.pool.RequiredType, slotCtrl.ShowLabel, slotCtrl.ShowImage);
    }

    /// <summary>
    /// This is the Main SetContent. 
    /// </summary>
    /// <param name="label">The name of the content</param>
    /// <param name="classInstance">The Instance of the class of that content.</param>
    /// <param name="requiredType">When provided, sets the required content type.</param>
    public bool SetContent(IContentInstance classInstance, GameObject itemObject) {

      PanelCtrl = GetComponentInParent<PanelController>();
      label = "";
      sprite = null;
      this.classInstance = null;

      if ( classInstance != null ) {
        label = classInstance.Name;
        sprite = classInstance.Sprite;
        this.classInstance = classInstance;
        pool.poolType = classInstance.SlotPool;
      }
      this.ItemObject = itemObject;

      return true;
    }

    /// <summary>
    /// Initialize the a Empty Slot.
    /// </summary>
    /// <param name="label">The name of the content</param>
    /// <param name="classInstance">The Instance of the class of that content.</param>
    /// <param name="requiredType">When provided, sets the required content type.</param>
    public bool SetContent(IContentInstance classInstance, GameObject itemObject, Type requiredType, bool showLabel, bool showImage) {

      bool canApply = SetContent(classInstance, itemObject);
      if ( !canApply )
        return false;

      this.ShowImage = showImage;
      this.ShowLabel = showLabel;
      pool.RequiredType = requiredType ?? pool.RequiredType;
      Init();

      return canApply;
    }
    
    /// <summary>
    /// Checks if the slots requirement is met for the classinstance and sets initialize the slot.
    /// The InventorySystem in a way where only the slot are interractable. The content of the slot
    /// being only an Image or a label. The slot knows What type of content it keeps (if any).
    /// In other Words, we can say that the Slot and the Content are one and the same.
    /// </summary>
    /// <param name="label">The label of the content of that slot.</param>
    /// <param name="classInstance">the Type of content that slot can hold.</param>
    /// <returns>True when the content information has been succesfully saved in the slot.</returns>
    public bool CheckAndSetContent(IContentInstance classInstance, GameObject gameObj) {

      if ( !pool.RequiredType.IsAssignableFrom(classInstance.GetType()) ) {
        Debug.Log("Inacceptable ClassInstance " + classInstance.GetType().ToString() + ". Should be " + pool.RequiredType.ToString());
        return false;
      }

      if ( !SetContent(classInstance, gameObj) )
        return false;

      Init();
      return true;
    }

    /// <summary>
    /// Apply stored info from the Slot into the visual of the item itself.
    /// </summary>
    public void ApplyItem() {

      PanelCtrl = GetComponentInParent<PanelController>();
      if ( text != null )
        text.text = label;
      if ( image != null )
        image.sprite = sprite;
      
      RectTransform rect = ItemObject.GetComponent<RectTransform>();
      rect.anchorMin = new Vector2(0f, 0f);
      rect.anchorMax = new Vector2(1f, 1f);
      rect.offsetMax = new Vector2(0f, 0f);
      rect.offsetMin = new Vector2(0f, 0f);

    }

    /// <summary>
    /// Creates an item under in the Slot.
    /// </summary>
    /// <param name="gameObjTemplate"></param>
    /// <param name="classInstance"></param>
    /// <returns></returns>
    public GameObject CreateItem(GameObject gameObjTemplate, IContentInstance classInstance) {
      return CreateItem(classInstance);
    }

    /// <summary>
    /// Creates an Item under the slot, based on the slot content.
    /// </summary>
    /// <param name="contentInstance"></param>
    /// <returns></returns>
    public GameObject CreateItem(IContentInstance contentInstance) {

      GameObject newItem = null;
      if ( ItemObject == null )
        newItem = Pooler.Instance.GetPooledObject(EnumPool.Item, gameObject.transform);
      else
        newItem = CreateObject();

      if ( !CheckAndSetContent(contentInstance, newItem) ) {
        Pooler.Instance.RecycleObjectAndChildren(newItem);
        return null;
      }

      ApplyItem();
      return newItem;
    }

    public GameObject CreateObject() {
      GameObject newItem = Pooler.Instance.GetPooledObject(EnumPool.Item, gameObject.transform);
      newItem.name = ClassInstance.Name + "-Item";
      ApplyItem();
      return newItem;
    }

    public GameObject CreateItem(SlotController sourceSlot) {
      if ( sourceSlot.IsEmpty )
        return null;

      return CreateItem(sourceSlot.ItemObject, sourceSlot.classInstance);
    }

    /// <summary>
    /// Removes all data from the Slot and destroy the child gameObject, which should be the content of the slot.
    /// </summary>
    public void ResetSlot() {
      Pooler.Instance.RecycleChildren(gameObject);
      SetContent(null, null);
    }

    /// <summary>
    /// Creates a new UI-Object that will follow the mouse. That new object contains the Data stored in this Slot (ClassInstance)
    /// so it can be passed to the next object.
    /// </summary>
    /// <param name="data">a PointerEventData data when the click occured.</param>
    /// <param name="containerSlot">The parent GameObject </param>
    /// <returns>a Dragging Input Context</returns>
    public InputContextDragging CreateDraggingObject(PointerEventData data) {
      Vector2 offset = data.position - (Vector2)transform.position;
      InputContextDragging newContext = new InputContextDragging(this, offset);
      return newContext;
    }

  }
}