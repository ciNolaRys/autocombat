﻿using Game.Characters.Behaviors;
using Game.Characters.Behaviors.Conditions;
using Game.Controller.InputContexts;
using Game.Spells;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Game.Util;

namespace Game.Controller.PanelControllers {
  public class PanelControllerBehavior : PanelControllerBehaviorDrawer {


    public int numberOfOptions = 10;

    private Dictionary<int, Behavior> options = new Dictionary<int, Behavior>();
    private Dictionary<int, GameObject> behaviorSlotList = new Dictionary<int, GameObject>();
    private InputContextDragging context;

    /// <summary>
    /// Builds the pannel and instanciates a number of slots for a later usage.
    /// </summary>
    void Start() {
      Init();
      // Create a number of possible empty Behaviors.
      for ( int i = 0; i < numberOfOptions; i++ ) {

        GameObject row = Pooler.Instance.AddPoolObj(EnumPool.BehaviorRow);

        // set container size for the scroll bar.
        rect.offsetMin += new Vector2(0, -36);
        row.name = i.ToString();

        row.transform.position = Vector2.zero;
        // initialize the content of each slots.

      }
    }

    void Update() {
      if ( !( SceneController.Instance.Context is InputContextDragging ) )
        return;

      context = (InputContextDragging)SceneController.Instance.Context;
      context.FollowMouse();
    }

    public override void ClearUI() {

      behaviorSlotList.Clear();
      base.ClearUI();

    }

    /// <summary>
    /// Create all the required slot for the behavior engine. This includes
    /// BehaviorSlots, ConditionSlots and ActionSlots
    /// All slots are to be populated with the current selected user data.
    /// The current selected character should contains a specific Behavior, Condition and Action sets.
    /// </summary>
    protected override void BuildContent() {

      int behaviorCount = engine.Behaviors.Count;

      for ( int i = 0; i < numberOfOptions; i++ ) {
        GameObject behaviorRow = Pooler.Instance.GetPooledObject(EnumPool.BehaviorRow, transform);

        behaviorSlotList.Add(i, behaviorRow);

        GameObject cond = Pooler.Instance.GetPooledObject(EnumPool.ConditionSlot, behaviorRow.transform);
        GameObject action = Pooler.Instance.GetPooledObject(EnumPool.ActionSlot, behaviorRow.transform);

        SlotController conditionSlot = cond.GetComponent<SlotController>();
        SlotController spellSlot = action.GetComponent<SlotController>();

        conditionSlot.SetContent(null, null, typeof(Condition), true, false);
        spellSlot.SetContent(null, null, typeof(SpellFactory), true, false);


        if ( i >= behaviorCount )
          continue;

        SpellFactory spell = engine.Behaviors[i].ability;
        Condition condition = engine.Behaviors[i].condition;

        if ( spell != null ) {
          spellSlot.PoolType = EnumPool.ActionSlot;
          spellSlot.CreateItem(spell);
          spellSlot.ApplyItem();
        }

        if ( condition != null ) {
          conditionSlot.PoolType = EnumPool.ConditionSlot;
          conditionSlot.CreateItem(condition);
          conditionSlot.ApplyItem();
        }
        
      }

    }

    /// <summary>
    /// Rebuild the complete behavior Dictionnary
    /// </summary>
    public override void UpdateSlots() {
      Canvas.ForceUpdateCanvases();

      foreach ( KeyValuePair<int, GameObject> itm in behaviorSlotList ) {
        Condition cond = (Condition)itm.Value.transform.GetChild(0).gameObject.GetComponent<SlotController>().ClassInstance;
        SpellFactory ability = (SpellFactory)itm.Value.transform.GetChild(1).gameObject.GetComponent<SlotController>().ClassInstance;
        options[itm.Key] = new Behavior(cond, ability);
      }

      SceneController.Instance.Context.Executor.Engine.UpdateBehavior(options);

    }

  }
}