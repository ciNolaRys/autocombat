﻿using Game.Util;
using System;
using UnityEngine;

namespace Game.Controller.PanelControllers {
  public abstract class PanelController : MonoBehaviour {

    public bool isItemDestroyable;
    public bool isSwappable;

    public abstract void Init();
    protected abstract void BuildContent();
    public abstract void ClearUI();
    public virtual GameObject InitItem() {
      return null;
    }
    public virtual GameObject InitSlot(EnumPool pool, string label) {
      GameObject slot = Pooler.Instance.GetPooledObject(pool);
      slot.name = label;  // this is only for the Hierarchy view...       
      return slot;
    }

    public virtual void UpdateSlots() { }
    public virtual bool CanApply(IContentInstance content) {
      return isItemDestroyable || isSwappable;
    }


  }
}
