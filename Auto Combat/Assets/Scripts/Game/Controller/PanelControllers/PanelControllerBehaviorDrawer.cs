﻿using Game.Characters.Behaviors;
using Game.Characters.Behaviors.Conditions;
using Game.Spells;
using Game.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Controller.PanelControllers {
  public abstract class PanelControllerBehaviorDrawer : PanelController {

//#pragma warning disable CS0649
//    [SerializeField]
//    protected GameObject slotObj;
//    [SerializeField]
//    protected GameObject itemObj;
//#pragma warning restore CS0649

    protected BehaviorEngine engine;
    protected RectTransform rect;


    /// <summary>
    /// Initializer for the slots. can receive a condition or an Action.
    /// </summary>
    /// <param name="pool"></param>
    /// <param name="label"></param>
    /// <param name="parent"></param>
    /// <returns></returns>
    public override GameObject InitSlot(EnumPool pool, string label) {

      GameObject slot = base.InitSlot(pool,label);
      InitItem();
      
      return slot;
    }

    public override GameObject InitItem() {
      GameObject item = Pooler.Instance.GetPooledObject(EnumPool.Item);
      return item;
    }

    public override void Init() {
      rect = GetComponent<RectTransform>();
    }

    public virtual void BuildContent(BehaviorEngine engine) {
      ClearUI();
      this.engine = engine;
      BuildContent();
    }

    public override void ClearUI() {
      rect.offsetMin = new Vector2(0, rect.offsetMax.y);
      Pooler.Instance.RecycleChildren(gameObject);
    }



  }
}
