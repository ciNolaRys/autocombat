﻿using Game.Characters;
using Game.Data;
using Game.Spells;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;
using System;
using UnityEngine;

namespace Game.Controller.PanelControllers {
  class PanelControllerInfo : PanelControllerSquad {

    //Layout stuff;
    public float padTop = 5f;
    public float padBottom = 5f;
    public float padRight = 10f;
    public float padLeft = 10f;
    public float spacing = 10f;

    private List<InfoElementController> infoController;
    private IContentInstance contentManager;
    
    private float width;
    private float height;

    protected override void BuildContent() {

      if ( contentManager == null ) {
        RemoveContent();
        return;
      }
      // notes
      // The content window is used for the tooltip and in the SquadBuilder.
      // so the lists it can get is either a list of stats to display
      // or a list of spell.

      if ( contentManager is CharacterFactory )
        BuildCharacterInfo();
    }

    /// <summary>
    /// Methods specific to build the Character Info Pannel.
    /// </summary>
    private void BuildCharacterInfo() {
      CharacterFactory characterFactory = (CharacterFactory)contentManager;
      AbilityList al = AbilityDatabaseDAO.LoadResource();
      List<SpellFactory> abilities = al.spellList;

      for ( int i = 0; i < characterFactory.abilities.Count; i++ ) {
        SpellFactory spell = abilities.Find(x => x.id == characterFactory.abilities[i]);
        infoController[i].SetContent(spell.Description,spell.icon);
      }
      infoController[0].text.text = contentManager.Description;
    }
    
    /// <summary>
    /// Call this method when a selected character is changed.
    /// </summary>
    /// <param name="characterFactory"></param>
    public void SetCharacter(IContentInstance contentManager) {
      this.contentManager = contentManager;
      BuildContent();
    }
    
    /// <summary>
    /// Removes all content inside this pannel. 
    /// </summary>
    private void RemoveContent() {
      foreach ( InfoElementController spellController in infoController ) {
        spellController.RemoveContent();
      }      
    }

    public override void Init() {
      width = GetComponent<RectTransform>().rect.width;
      height = GetComponent<RectTransform>().rect.height;

      infoController = new List<InfoElementController>();
      BuildContent();
    }

    void LateUpdate() {

      int childCount = transform.childCount;

      for (int i =0; i<childCount;i++ ) {
        float yPos = 0f;
        float xPos = 0f;
        RectTransform previousSiblingRect = null;
        RectTransform childRect = transform.GetChild(i).GetComponent<RectTransform>();
        RectTransform textRect = childRect.GetComponentInChildren<Text>().gameObject.GetComponent<RectTransform>();
        childRect.sizeDelta = new Vector2(childRect.rect.width, textRect.rect.height);

        if ( i > 0 ) {
          previousSiblingRect = transform.GetChild(i - 1).GetComponent<RectTransform>();
          yPos = previousSiblingRect.anchoredPosition.y + previousSiblingRect.rect.y;
          xPos = previousSiblingRect.rect.x;
        }

        childRect.anchoredPosition = new Vector2(xPos + padRight, yPos - spacing);

      }
    }

  }
}
