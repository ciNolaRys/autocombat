﻿using Game.Util;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Controller.PanelControllers {
  public abstract class PanelControllerSquad : PanelController {
    
    protected SquadCanvasController squadCtrl;
    protected List<SlotController> slots = new List<SlotController>();

    public override void Init() {
      
      squadCtrl = gameObject.GetComponentInParent<SquadCanvasController>();
      ClearUI();
      BuildContent();
    }

    public void ResetHighlight() {

      foreach ( SlotController slot in slots ) {
        slot.UnselectItem();
      }
    }

    /// <summary>
    /// Recycle all objects under this panel.
    /// </summary>
    public override void ClearUI() {
      for ( int i = transform.childCount - 1; i >= 0; i-- ) {
        Pooler.Instance.RecycleObjectAndChildren(transform.GetChild(i).gameObject);
      }
    }

  }
}
