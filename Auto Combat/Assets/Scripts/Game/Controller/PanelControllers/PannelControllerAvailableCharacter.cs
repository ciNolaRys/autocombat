﻿using Game.Characters;
using Game.Data;
using Game.Users;
using Game.Util;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Controller.PanelControllers {
  class PannelControllerAvailableCharacter : PanelControllerSquad {

    

    /// <summary>
    /// Instanciate and pools object for the Available CharacterList.
    /// </summary>
    void Start() {
      CharacterList characterList = CharacterDatabaseDAO.LoadResource();      
      foreach ( CharacterFactory character in characterList.characterList ) {        
        InitSlot(character.SlotPool, "Slot-Character");
      }
    }

    /// <summary>
    /// Build the Available Characters for that User to select.
    /// </summary>
    protected override void BuildContent() {
      CharacterList characterList = CharacterDatabaseDAO.LoadResource();
      List<UserCharacter> userCharacters = squadCtrl.User.Characters;
      slots.Clear();

      foreach ( UserCharacter UserCharacter in userCharacters ) {
        GameObject slot = Pooler.Instance.GetPooledObject(EnumPool.CharacterSlot,transform);
        CharacterFactory cf = characterList.GetItemById(UserCharacter.CharacterId);
        SlotController slotController = slot.GetComponent<SlotController>();
        slots.Add(slotController);

        slotController.SetContent(cf, null, cf.GetType(), false, true);
        GameObject item = slotController.CreateItem(cf);
        
      }
      
    }

  }
}
