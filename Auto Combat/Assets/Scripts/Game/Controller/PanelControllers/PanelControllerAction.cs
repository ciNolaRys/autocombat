﻿using Game.Spells;
using Game.Util;
using UnityEngine;

namespace Game.Controller.PanelControllers {
  class PanelControllerAction : PanelControllerBehaviorDrawer {

    private int maxAction = 6;

    void Start() {

      for ( int i = 0; i < maxAction; i++ ) {
        GameObject slot = InitSlot(EnumPool.ActionSlot, "Slot-Spell");
        Pooler.Instance.AddPoolObj(EnumPool.ActionSlot, slot);
      }
      Init();
    }

    protected override void BuildContent() {
      
      foreach ( SpellFactory ability in engine.abilityList ) {

        GameObject actionSlot = Pooler.Instance.GetPooledObject(EnumPool.ActionSlot, transform);
        SlotController slot = actionSlot.GetComponent<SlotController>();
        slot.SetContent(ability, null, ability.GetType(), true, false);
        GameObject action = slot.CreateItem(ability);        
        action.transform.position = actionSlot.transform.position;
        slot.ApplyItem();
        rect.offsetMin += new Vector2(0, -32);
      }
    }
    
  }
}
