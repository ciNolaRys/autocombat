﻿using Game.Characters;
using Game.Data;
using Game.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Game.Controller.PanelControllers {
  /// <summary>
  /// Class to control the SelectedCharacter Pannel specifically;
  /// </summary>
  class PanelControllerSelectedCharacter : PanelControllerSquad {


    private List<GameObject> characterSlots = new List<GameObject>();
    private int teamSize = 5;

    public override void Init() {
      base.Init();
      isSwappable = true;
      isItemDestroyable = true;
    }

    /// <summary>
    /// Creates slots for the Selected character panel, and assign Character if User has Character Selected.
    /// </summary>
    protected override void BuildContent() {

      for ( int i = 0; i < teamSize; i++ ) {

        GameObject characterSlot = Pooler.Instance.GetPooledObject(EnumPool.CharacterSlot, gameObject.transform);

        SlotController characterSlotCtrl = characterSlot.GetComponent<SlotController>();
        characterSlot.name = "SelectedSlot";
        Type type = typeof(CharacterFactory);
        characterSlots.Add(characterSlot);

        if ( squadCtrl.User.SelectedCharacters.ElementAtOrDefault(i) != null ) {
          CharacterFactory cf = CharacterDatabaseDAO.FindCharacterById(squadCtrl.User.SelectedCharacters[i].CharacterId);
          type = cf.GetType();
        }
        characterSlotCtrl.SetContent(null, null, type, false, true);
      }
    }


    public override bool CanApply(IContentInstance content) {

      foreach ( GameObject g in characterSlots ) {
        SlotController sc = g.GetComponent<SlotController>();
        if ( sc.ClassInstance != null && sc.ClassInstance.Equals(content) ) {
          return false;
        }
      }

      return base.CanApply(content);
    }


  }
}
