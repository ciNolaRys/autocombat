﻿using Game.Characters.Behaviors.Conditions;
using Game.Data;
using Game.Util;
using UnityEngine;

namespace Game.Controller.PanelControllers {
  public class PanelControllerCondition : PanelControllerBehaviorDrawer {

    private ConditionList conditionList;
    
    public void Start() {
      conditionList = ConditionDatabaseDAO.LoadResource();
      
      foreach (ConditionFactory c in conditionList.conditions) {
        GameObject slot = InitSlot(EnumPool.ConditionSlot, "Slot-Condition");
        Pooler.Instance.AddPoolObj(EnumPool.ConditionSlot, slot);
      }

      Init();
    }

    protected override void BuildContent() {
      
      foreach (ConditionFactory c in conditionList.conditions) {

        GameObject conditionSlot = Pooler.Instance.GetPooledObject(EnumPool.ConditionSlot,transform);        
        SlotController slotController = conditionSlot.GetComponent<SlotController>();
        Condition condition = c.InstaciateCondition();
        slotController.SetContent(condition, conditionSlot, condition.GetType(), true, false);
        slotController.CreateItem(condition);
        //slotController.SetContent(c.InstaciateCondition(), condition, c.GetType(),true,false);
        slotController.ApplyItem();
        rect.offsetMin += new Vector2(0,-32);
      }
    }

  }
}
