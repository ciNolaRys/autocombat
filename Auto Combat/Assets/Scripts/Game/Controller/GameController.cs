﻿using Game.Characters;
using Game.Users;
using Game.Util;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Game.Data;
using UnityEngine.Events;
using Game.Controller.InputContexts;
using Game.InventorySystem;
using Game.UI;

namespace Game.Controller {
  public class GameController:PersistantSingleton<GameController>{
    
    public static GameController Instance { get { return _instance; } }
    
    public bool isDev;
    public Canvas sceneMenuCanvas;
    public Slider loadingBar;
    public GameObject loadingImage;    
    public GameObject squadMenuCanvas;
    public GameObject mainMenuCanvas;

    private List<Character> squad;
    private User user;
    private AsyncOperation async;
    private float time=1;    
    private Dictionary<string, UnityEvent> eventDictionary;
    
    void Start() {
      Init();
    }

    /// <summary>
    /// Game initializer.
    /// </summary>
    private void Init() {
      squadMenuCanvas.SetActive(false);
      eventDictionary = new Dictionary<string, UnityEvent>();
      SceneController.Instance.ResetContext();

      // DEV : remove this if one day. for dev only.
      if ( isDev )
        SceneController.Instance.SetContext(new InputContextMain());
      else
        SceneController.Instance.SetContext(new InputContextMainMenu());

      loadingImage.SetActive(false);
    }
    
    /// <summary>
    /// Opens the pause menu.
    /// </summary>
    public void OpenSceneMenu() {
      SceneController.Instance.SetContext(new InputContextSceneMenu());
      time = Time.timeScale;
      Time.timeScale = 0f;
      sceneMenuCanvas.gameObject.SetActive(true);
      sceneMenuCanvas.transform.SetAsLastSibling();
    }

    public void OpenSquadMenu() {
      if ( user == null )
        return;
      InputContextSquadMenu squadMenuContext = (InputContextSquadMenu) SceneController.Instance.SetContext(new InputContextSquadMenu(user));
    }
    
    /// <summary>
    /// Start the game for the selected User, at the latest save point.
    /// </summary>
    public void StartGame() {
      if ( user == null ) { return; }
    }

    /// <summary>
    /// Back Button functionnality. Consume the latest context, which hopefully will be the screen that the user is currently on.
    /// </summary>
    public void Back() {
      // TODO, ideally, this would know which context to consume. For that the button need to pass the context in which it lives.
      SceneController.Instance.ConsumeContext(SceneController.Instance.Context);
    }

    /// <summary>
    /// Store a new user into the database with defaul values.
    /// </summary>
    /// <param name="username">the username of the new user</param>
    public void CreateUser(string username) {

      User user = new User(username, null, null);
      UserList userlst = UserDatabaseDAO.LoadUserDatabase();
      UserDatabaseDAO.SaveNewUser(user, userlst);
      
    }

    /// <summary>
    /// Register an event in the event manager.
    /// </summary>
    /// <param name="eventName">The name of an event.</param>
    /// <param name="listener">The method that will get called when the event is triggered.</param>
    public void StartListening(string eventName, UnityAction listener) {
      UnityEvent thisEvent = null;
      if ( _instance.eventDictionary.TryGetValue(eventName, out thisEvent) ) {
        thisEvent.AddListener(listener);
      } else {
        thisEvent = new UnityEvent();
        thisEvent.AddListener(listener);
        _instance.eventDictionary.Add(eventName, thisEvent);
      }
    }

    /// <summary>
    /// Removes a event listener;
    /// </summary>
    /// <param name="eventName"></param>
    /// <param name="listener"></param>
    public void StopListening(string eventName, UnityAction listener) {
      UnityEvent thisEvent = null;
      if ( _instance.eventDictionary.TryGetValue(eventName, out thisEvent) ) {
        thisEvent.RemoveListener(listener);
      }
    }

    /// <summary>
    /// triggers an event, fireing the method attached.
    /// </summary>
    /// <param name="eventName"></param>
    public static void TriggerEvent(string eventName) {
      UnityEvent thisEvent = null;
      if ( _instance.eventDictionary.TryGetValue(eventName, out thisEvent) ) {
        thisEvent.Invoke();
      }
    }
    
    /// <summary>
    /// for online user registration.
    /// </summary>
    /// <param name="username"></param>
    /// <param name="password"></param>
    public void UserLogin(string username, string password) {
      UserList userlist = UserDatabaseDAO.LoadUserDatabase();
      User loggedInUser = userlist.userList.Find(x => x.Name == username);

      //if ( loggedInUser != null ) {
      //  LoadUser(loggedInUser);
      //}
      user = loggedInUser;
    }

    /// <summary>
    /// Keeps the selected user in memory.
    /// </summary>
    /// <param name="user"></param>
    public void SelectUser(User user) {
      this.user = user;
      mainMenuCanvas.GetComponentInChildren<UserSelection>().SelectUser(user);
    }
    
    /// <summary>
    /// resume the game. This removes a context, so be sure to add a context when the game pauses.
    /// </summary>
    public void ResumeGame() {
      Time.timeScale = time;
      sceneMenuCanvas.gameObject.SetActive(false);
      SceneController.Instance.SetContext(null);
    }

    /// <summary>
    /// Loads the MainMenu scene.
    /// </summary>
    public void ReturnToMainMenu() {
      SceneController.Instance.ResetContext();
      SceneController.Instance.SetContext(new InputContextMainMenu());
      sceneMenuCanvas.gameObject.SetActive(false);      
      SceneManager.LoadScene("MainMenu");
    }

    /// <summary>
    /// Exit the application.
    /// </summary>
    public void ExitGame() {
      Application.Quit();
    }

    /// <summary>
    /// Loads a sceneAsynchronously, displaying a progress bar.
    /// </summary>
    /// <param name="level"></param>
    public void LoadAsync(int level) {
      loadingImage.SetActive(true);
      loadingImage.transform.SetAsLastSibling();
      StartCoroutine(LoadLevelWithBar(level));
    }

    /// <summary>
    /// coroutine that shows the progress on the progress bar.
    /// </summary>
    /// <param name="level"></param>
    /// <returns></returns>
    IEnumerator LoadLevelWithBar(int level) {
      Time.timeScale = 1;
      async = SceneManager.LoadSceneAsync(level);
      while (!async.isDone) {
        loadingBar.value = async.progress;
        yield return null;
      }

      SceneController.Instance.ResetContext();
      loadingImage.SetActive(false);

    }

    
    private void LoadScene(int sceneNumber) {

    }

    
#if UNITY_EDITOR
    void OnGUI() {
      // dev : ongui that displays the current context chain.
      string context = SceneController.Instance.Context.ToString();     
      GUI.TextArea(new Rect(Screen.width - 200, 0, 198, 150),context);
    }
#endif


  }
}
