﻿using UnityEngine;

namespace Game.Util {
  public class Singleton<Instance>:MonoBehaviour
  where Instance : Singleton<Instance> {

    protected static Instance _instance;

    public virtual void Awake() {

      if (!_instance) {
        _instance = this as Instance;
      } else {
        DestroyObject(gameObject);
      }
    }
  }
}