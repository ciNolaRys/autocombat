﻿using UnityEngine;

namespace Game.Util {
  public static class Utils {

    /// <summary>
    /// Loop through the gameObject sharing a tag, and returns the Component<T> from
    /// the gameObject haveing the name objectName
    /// </summary>
    /// <typeparam name="T">Componant we are looking for</typeparam>
    /// <param name="tag">Searching Tag</param>
    /// <param name="objectName">Object name</param>
    /// <returns>The componant</returns>
    public static object findOneComponentByTag<T>(string tag, string objectName) where T : Object {
      foreach ( GameObject go in GameObject.FindGameObjectsWithTag(tag) ) {
        if ( go.name.Equals(objectName) ) {
          return go.GetComponent<T>();
        }
      }
      return null;
    }

    public static Transform findOneGameObjectInChildrenByName(string objectName, GameObject go) {

      Transform res = go.transform.Find(objectName);
      if ( res != null )
        return res;

      foreach ( Transform child in go.transform ) {
        res = findOneGameObjectInChildrenByName(objectName, child.gameObject);
        if ( res != null )
          return res;
      }
      return null;
    }

    /// <summary>
    /// Destroys all childrent gameObject from the specified GameObject.
    /// </summary>
    /// <param name="o">the object that want to kill its children</param>
    public static void DestroyGameObjectChildren(GameObject o) {

      int childCount = o.transform.childCount;
      Debug.Log("Destroying objects : " + o.name);
      for ( int i = 0; i < childCount; i++ ) {
        Object.Destroy(o.transform.GetChild(i).gameObject);
      }
    }

    public static Color ConvertColor(int red, int green, int blue, int alpha) {
      return new Color(red / 255f, green / 255f, blue / 255f, alpha / 255f);
    }

  }
}
