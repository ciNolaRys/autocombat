﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Util {
  /// <summary>
  /// An utility class that provide helpers for pooling objects in a dictionnary.
  /// Pooled Object are instanciated in a pooled Object. It's up to the caller to place it in the right hierarchy.
  /// </summary>
  public class Pooler {


    public static Pooler Instance {
      get {
        if ( instance != null )
          return instance;
        else {
          return new Pooler();
        }
      }
    }

    private Dictionary<EnumPool, GameObject> templateObjects;
    private static Pooler instance;
    private Dictionary<EnumPool, List<GameObject>> poolList;
    private GameObject poolObj;
    private Dictionary<GameObject,float> objectToRecycle = new Dictionary<GameObject, float>();


    private Pooler() {
      poolList = new Dictionary<EnumPool, List<GameObject>>();
      poolObj = new GameObject("GameObjectPool");
      instance = this;

      templateObjects = new Dictionary<EnumPool, GameObject>();

      templateObjects.Add(EnumPool.ActionSlot, (GameObject)Resources.Load("prefabs/UI/BehaviorUI/ActionSlot"));
      templateObjects.Add(EnumPool.BehaviorRow, (GameObject)Resources.Load("prefabs/UI/BehaviorUI/BehaviorRow"));
      templateObjects.Add(EnumPool.CharacterSlot, (GameObject)Resources.Load("prefabs/UI/EmptySlot"));
      templateObjects.Add(EnumPool.ConditionSlot, (GameObject)Resources.Load("prefabs/UI/BehaviorUI/ConditionSlot"));
      templateObjects.Add(EnumPool.Item, (GameObject)Resources.Load("prefabs/UI/EmptyItem"));
      templateObjects.Add(EnumPool.Empty, (GameObject)Resources.Load("prefabs/UI/EmptySlot"));

    }

    public GameObject AddPoolObj(EnumPool pool) {
      GameObject template = null;
      templateObjects.TryGetValue(pool, out template);
      GameObject obj = UnityEngine.Object.Instantiate(template);
      return AddPoolObj(pool, obj, false);
    }

    /// <summary>
    /// Add a new resource into the pool
    /// </summary>
    /// <param name="pool"></param>
    /// <param name="obj"></param>
    public GameObject AddPoolObj(EnumPool pool, GameObject obj, bool isUsed = false) {

      obj.SetActive(isUsed);
      if ( poolList.ContainsKey(pool) )
        poolList[pool].Add(obj);
      else {
        List<GameObject> g = new List<GameObject>();
        g.Add(obj);
        poolList.Add(pool, g);
      }

      if ( !isUsed ) {
        obj.transform.SetParent(poolObj.transform);
      }
      
      return obj;
    }

    /// <summary>
    /// Get one pooled object and sets it active. Will add a new one if a template is available.
    /// </summary>
    /// <param name="pool"></param>
    /// <returns></returns>
    public GameObject GetPooledObject(EnumPool pool, Transform parent = null) {

      List<GameObject> onePool;
      GameObject res = null;

      if ( poolList.TryGetValue(pool, out onePool) ) {
        foreach ( GameObject obj in onePool ) {
          if ( !obj.activeInHierarchy ) {
            res = obj;
            break;
          }
        }
      }

      if ( res == null ) {
        GameObject template = null;
        if ( templateObjects.TryGetValue(pool, out template) ) {
          GameObject o = UnityEngine.Object.Instantiate(template);
          AddPoolObj(pool, o);
          res = o;
        }
      }

      if ( res != null ) {
        if ( parent != null ) {
          res.transform.SetParent(parent, false);
          res.SetActive(true);
        }
        else {
          res.transform.SetParent(poolObj.transform, false);
          res.SetActive(false);
        }
      }

      return res;
    }

    /// <summary>
    /// Gets a pooled object and create one based on a template if none is available.
    /// </summary>
    /// <param name="pool"></param>
    /// <param name="obj"></param>
    /// <param name="parent"></param>
    /// <returns></returns>
    public GameObject GetPooledObject(EnumPool pool, GameObject obj, Transform parent = null) {

      GameObject res = GetPooledObject(pool, parent);

      if ( res == null ) {
        GameObject o = UnityEngine.Object.Instantiate(obj);
        res = AddPoolObj(pool, o, true);
        if ( parent != null )
          res.transform.SetParent(parent, false);
      }

      return res;

    }

    /// <summary>
    /// Recursively Recycle this object and all its children
    /// </summary>
    /// <param name="pool"></param>
    /// <param name="obj"></param>
    public void RecycleObjectAndChildren(GameObject obj) {

      Pool pool = obj.GetComponent<Pool>();
      int childCount = obj.transform.childCount - 1;

      while ( childCount >= 0 ) {
        RecycleObjectAndChildren(obj.transform.GetChild(childCount).gameObject);
        childCount--;
      }
      if ( pool != null ) {
        obj.transform.SetParent(poolObj.transform, false);
        obj.SetActive(false);        
      }

    }

    /// <summary>
    /// Recycle Children objects, leaving the main object untouched.
    /// </summary>
    /// <param name="obj"></param>
    public void RecycleChildren(GameObject obj) {

      int childCount = obj.transform.childCount;
      int i = childCount-1;
      while ( i >= 0 ) {
        RecycleObjectAndChildren(obj.transform.GetChild(i).gameObject);
        i--;
      }
    }


  }
}
