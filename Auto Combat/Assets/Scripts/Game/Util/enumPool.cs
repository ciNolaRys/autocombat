﻿namespace Game.Util {
  public enum EnumPool {

    None,
    ActionSlot,
    ConditionSlot,
    CharacterSlot,
    BehaviorRow,
    UserRow,
    Item,
    Spell,
    Empty
  } 
}