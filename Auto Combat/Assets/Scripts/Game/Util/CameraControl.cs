﻿using Game.Controller;
using Game.Controller.InputContexts;
using UnityEngine;

namespace Game.Util {
  public class CameraControl : MonoBehaviour {

    public float zoomSpeed = 20f;
    private float minCamSize = 5f;
    private float maxCamSize = 20f;
    private float xMovement;
    private float zMovement;
    private float camMovementSpeed = 1;
    private float camScrollSpeed = 1;

#pragma warning disable
    private float screenWidth;
    private float screenHeight;
#pragma warning restore

    protected InputContext inputController;


    void Start() {
      inputController = SceneController.Instance.Context;
      screenHeight = Screen.height;
      screenWidth = Screen.width;
    }

    // Update is called once per frame
    protected virtual void LateUpdate() {

      if ( inputController.MouseMiddleClick() )
        //CameraTranslation();
        Cursor.visible = false;
      else
        Cursor.visible = true;
      /* dont forget to remove the comments here when you build the game!!
      if (Input.mousePosition.x < .5f)
          ScrollLeft();
      if (Input.mousePosition.y < .5f)
          ScrollDown();
      if (Input.mousePosition.y > screenHeight - .5f)
          ScrollUp();
      if (Input.mousePosition.x > screenWidth - .5f)
          ScrollRight();
      */
    }


    private void ScrollDown() {
      Camera.main.transform.Translate(Vector3.back * 3 * camScrollSpeed, Space.World);
    }
    private void ScrollLeft() {
      Camera.main.transform.Translate(Vector3.left * camScrollSpeed, Space.World);
    }
    private void ScrollUp() {
      Camera.main.transform.Translate(Vector3.forward * 3 * camScrollSpeed, Space.World);
    }
    private void ScrollRight() {
      Camera.main.transform.Translate(Vector3.right * camScrollSpeed, Space.World);
    }

    public void CameraTranslation() {
      Cursor.visible = false;
      xMovement = -Input.GetAxis("Mouse X") * camMovementSpeed;
      zMovement = -Input.GetAxis("Mouse Y") * 3 * camMovementSpeed;
      Vector3 movement = new Vector3(xMovement, 0, zMovement);
      Camera.main.transform.Translate(movement, Space.World);
    }

    public void ZoomForward() {
      float newDist = Mathf.Min(Camera.main.orthographicSize, minCamSize);
      Camera.main.orthographicSize = Mathf.Lerp(Camera.main.orthographicSize, newDist, zoomSpeed * Time.deltaTime * .5f);
    }

    public void ZoomBackward() {
      float newDist = Mathf.Max(Camera.main.orthographicSize, maxCamSize);
      Camera.main.orthographicSize = Mathf.Lerp(Camera.main.orthographicSize, newDist, zoomSpeed * Time.deltaTime * .1f);
    }




  }

}