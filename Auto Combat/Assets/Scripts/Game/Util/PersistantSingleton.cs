﻿using UnityEngine;

namespace Game.Util {
  public class PersistantSingleton<Instance>:MonoBehaviour
  where Instance : PersistantSingleton<Instance> {

    
    protected static Instance _instance;

    public virtual void Awake() {

      if (!_instance) {
        _instance = this as Instance;
      } else {
        DestroyObject(gameObject);
      }
      DontDestroyOnLoad(gameObject);

    }
  } 
}