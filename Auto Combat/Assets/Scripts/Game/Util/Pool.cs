﻿using UnityEngine;
using System.Collections;
using System;
using Game.Spells;
using Game.Characters.Behaviors.Conditions;

namespace Game.Util {
  public class Pool : MonoBehaviour {

    public EnumPool poolType;

    public Type RequiredType {
      get {

        if ( requiredType != null )
          return requiredType;

        switch ( poolType ) {
          case EnumPool.ActionSlot:
            return typeof(SpellFactory);
          case EnumPool.ConditionSlot:
            return typeof(Condition);
          default:
            return null;

        }
      }
      set {
        requiredType = value;
      }
      

    }


    private Type requiredType;
  }

}
