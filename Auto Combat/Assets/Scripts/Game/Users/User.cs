﻿using Game.Characters;
using Game.Characters.Behaviors.Conditions;
using Game.Controller;
using Game.Data;
using Game.InventorySystem;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Users {
  [Serializable]
  public class User:IDatabase {

    public Guid Id { get { return id; } }
    public string Name { get { return userName; } }
    public string Password { get { return password; } }
    public List<UserCharacter> Characters { get { return characters; } }
    public List<UserCharacter> SelectedCharacters { get { return selectedCharacters; } }

    private Guid id = new Guid();    
    private string userName;
    private string password;
    private string email;    
    /// <summary>
    /// All available Characters
    /// </summary>
    private List<UserCharacter> characters;
    /// <summary>
    /// A List containing all character currently in the squad.
    /// </summary>
    private List<UserCharacter> selectedCharacters;
    /// <summary>
    /// All available conditions
    /// </summary>
    private List<Condition> conditions;
    /// <summary>
    /// The global inventory of the user (items one in a character)
    /// </summary>
    private Inventory globalInventory;
    /// <summary>
    /// This will surely change for SceneData.
    /// </summary>
    private List<SceneController> sceneSaves;
    /// <summary>
    /// Default Contructor for an empty User. this is only used to Create the UserDatabase...
    /// </summary>
    public User() { }
   
    /// <summary>
    /// Contructor to create a new user Initialize with the User info and Builds the Main Chanracter.
    /// </summary>
    /// <param name="userName">the name of the user</param>
    /// <param name="password">the password of the user</param>
    /// <param name="email">the email of the user</param>
    public User(string userName,string password,string email) {
      id = Guid.NewGuid();
      this.userName = userName;
      this.password = password;
      this.email = email;            

      CharacterList cl = CharacterDatabaseDAO.LoadResource();

      characters = new List<UserCharacter>();
      selectedCharacters = new List<UserCharacter>();
      CharacterFactory characterFactory = cl.getItemByName("Bingeamin");
      characters.Add(new UserCharacter(characterFactory.id));
      characterFactory = cl.getItemByName("Evil ColsShower");
      characters.Add(new UserCharacter(characterFactory.id));

    }
    
    /// <summary>
    /// clone a copy of that ability. 
    /// </summary>
    /// <returns></returns>
    public object GetCopy() {
      return (User)MemberwiseClone();
    }

  }
}