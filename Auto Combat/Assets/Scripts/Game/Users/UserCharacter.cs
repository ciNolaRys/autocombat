﻿using Game.Characters;
using Game.Characters.Behaviors;
using Game.InventorySystem;
using Game.Spells;
using System;
using System.Collections.Generic;

namespace Game.Users {
  /// <summary>
  /// The userCharacter is the state of one character for one user. Contains specific Spell, stats, condition?, Inventory for that Character.
  /// </summary>
  [Serializable]
  public class UserCharacter {

    public int CharacterId { get; set; }
    public CharacterStats Stats { get; set; }
    //public BehaviorGroup BehaviorGroup { get; set; }
    public List<Guid> Spells { get; set; }
    //public Inventory Inventory { get; set; }


    public UserCharacter(int characterId, 
      CharacterStats stats=null, BehaviorGroup behaviorGrp = null, List<Guid> spells = null, Inventory inventory = null) {

      CharacterId = characterId;
      Stats = stats;
      //BehaviorGroup = behaviorGrp;
      Spells = spells;
      //Inventory = inventory;
    }
  }
}
