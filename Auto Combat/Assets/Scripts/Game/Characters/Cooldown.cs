﻿using Game.Spells;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Characters {
  public class Cooldown:MonoBehaviour {

    private Dictionary<string,float> cooldownList = new Dictionary<string, float>();
    
    public void Update() {
      Dictionary<string,float> newCoolDownList = new Dictionary<string,float>();
      // Refresh Cooldown timers
      foreach (KeyValuePair<string,float> cd in cooldownList) {
        float time = cooldownList[cd.Key] - Time.deltaTime;
        
        if (time > 0) {
          newCoolDownList.Add(cd.Key,time);
        }
      }
      //override the original dict with the new values, (without the finished cooldowns.)
      cooldownList = new Dictionary<string, float>(newCoolDownList);
    }

    
    public bool isReady(string abilityName) {
      if (cooldownList.ContainsKey(abilityName))
        return false;
      return true;
    }

    public float setCooldown(SpellFactory ability) {
      cooldownList.Add(ability.name, ability.cooldown);
      return ability.cooldown + Time.time;
    }

  }
}