﻿
using Game.Util;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Characters {
  public class MovementController : MonoBehaviour {


    public float speed = 20f;

    private Rigidbody playerRigidBody;
    private Animator anim;
    private Vector3 moveDestination;
    private Vector3 movement;
    private bool isMoving;
    private LayerMask layer;

    private float closeEnoughDist = .5f;
    private float speedFactor = 2;
    private CameraFacing camFace;
    
    private void Start() {
      camFace = GetComponentInChildren<CameraFacing>();
      playerRigidBody = GetComponent<Rigidbody>();
      anim = GetComponentInChildren<Animator>();
      layer = LayerMask.GetMask("Floor");
    }

    public void setDestination(Vector3 hitPosition) {
      moveDestination = hitPosition;
      moveDestination.y = 0;
      isMoving = true;
    }

    public void FixedUpdate() {
      RunFixedUpdate();
    }

    private void RunFixedUpdate() {
      Vector3 step;

      if ( isMoving ) {
        step = moveDestination - transform.position;
        move(step.x, step.z * speedFactor);

        Vector3 dest = new Vector3(moveDestination.x, 0f, moveDestination.z);
        Vector3 curr = new Vector3(transform.position.x, 0f, transform.position.z);

        if ( Vector3.Distance(dest,curr) <= closeEnoughDist ) {
          isMoving = false;
        }
      }
      else {
        step.x = 0;
        step.y = 0;
      }
    }

    /// <summary>
    /// Set the Destination Vector3 and lookDirection (leftRight).
    /// </summary>
    /// <param name="hitPosition">The place that has been hit</param>
    public void SetDestination(Vector3 hitPosition) {
      if ( Vector3.Distance(moveDestination, hitPosition) <= closeEnoughDist )
        return;
      moveDestination = hitPosition;

      Vector3 relativePosition = transform.InverseTransformPoint(moveDestination);
      if ( relativePosition.x > 0 )
        LookRight();
      else
        LookLeft();

      isMoving = true;
    }

    /// <summary>
    /// Sets the Correct value on the CameraFacing script
    /// </summary>
    public void LookRight() {
      camFace.reverseFace = false;
    }
    /// <summary>
    /// Sets the Correct value on the CameraFacing script
    /// </summary>
    public void LookLeft() {
      camFace.reverseFace =true;
    }

    private void move(float x, float y) {
      movement.Set(x, 0f, y);
      movement = movement.normalized * ( speed * 0.5f ) * Time.deltaTime;
      playerRigidBody.MovePosition(transform.position + movement);
    }
  }

}