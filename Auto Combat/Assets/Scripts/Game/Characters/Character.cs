﻿using Game.Spells;
using Game.Characters.Behaviors;
using Game.Controller;
using Game.InventorySystem;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Game.Characters {
  /// <summary>
  /// Class that holds character data. Contains function that affects the character data and animations.
  /// </summary>
  public class Character : MonoBehaviour {

    public long freq;
    public long timer;

    public WorldView WorldView {
      get {
        return worldView;
      }
    }
    public BehaviorEngine Engine {
      get {
        return engine;
      }
    }
    public Inventory Inventory {
      get {
        return inventory;
      }
    }
    public CharacterUI TimeFillers {
      get { return timeFillUI; }
    }
    public CharacterStats Stat {
      get { return stat; }
    }
    public MovementController MoveController {
      get {
        return moveController;
      }
    }
    public GameObject SkillGameObject {
      get { return skillGameObject; }
    }
    public GameObject SelectObject {
      get { return selectObject; }
    }
    public GameObject Weapon {
      get { return weapon; }
    }
    public GameObject ActionPoint {
      get { return actionPoint; }
    }

    public string CharacterName {
      get {
        return characterName;
      }
    }

    // Following var should be determined by weapon
    public float hitRange = 3f;
    public float hitAmount = 10f;
    // end remove these
    // not sure about these
    public Guid id = Guid.NewGuid();
    private Animator animator;

    private WorldView worldView;
    private BehaviorEngine engine;
    private Inventory inventory;
    private CharacterUI timeFillUI;
    private CharacterStats stat = new CharacterStats();
    private MovementController moveController;
    private GameObject skillGameObject;
    private GameObject selectObject;
    private GameObject weapon;
    private GameObject actionPoint;

    private bool isAlreadyDead = false;
    private string characterName;
    public string teamName;


    protected List<OverTimeEffects> effectOverTime;
    public Sprite avatar;

    /// <summary>
    /// This Struct is to control the Sortig order of character related sprites.
    /// </summary>
    struct rendererStruct {
      public SpriteRenderer renderer;
      public int sortingOffset;
      public rendererStruct(SpriteRenderer r, int offset) {
        renderer = r;
        sortingOffset = offset;
      }
    }
    private List<rendererStruct> renderers = new List<rendererStruct>();

    void Start() {

      var AllCharacterObjects = GetComponentsInChildren<Transform>().ToList();

      SceneController.Instance.RegisterTeamMember(this, teamName);
      moveController = GetComponent<MovementController>();
      selectObject = AllCharacterObjects.Find(x => x.gameObject.name.Equals("Selected")).gameObject;
      skillGameObject = AllCharacterObjects.Find(x => x.gameObject.name.Equals("SKILL")).gameObject;
      weapon = AllCharacterObjects.Find(x => x.gameObject.name.Equals("wepon")).gameObject;
      actionPoint = AllCharacterObjects.Find(x => x.gameObject.name.Equals("ActionPoint")).gameObject;
      engine = GetComponent<BehaviorEngine>();

      worldView = GetComponent<WorldView>();
      worldView.ThisCharacter = this;

      timeFillUI = GetComponent<CharacterUI>();
      animator = gameObject.GetComponentInChildren<Animator>();
      inventory = new Inventory(stat);

      selectObject.SetActive(false);

      List<SpriteRenderer> rs = gameObject.GetComponentsInChildren<SpriteRenderer>().ToList();
      foreach ( SpriteRenderer r in rs ) {
        rendererStruct mr = new rendererStruct(r, r.sortingOrder);
        renderers.Add(mr);
      }
    }

    void LateUpdate() {
      // Draw the health Slider smoothly
      timeFillUI.SetHealthBarFill(stat.BaseHealth, stat.Health);
      timeFillUI.setHealthTransitionFill(
        stat.BaseHealth,
        stat.ProgressHealth = Mathf.Lerp(
          stat.ProgressHealth,
          stat.Health, Time.deltaTime * 3));

      timeFillUI.InitiativeFill(engine.InitialInitiative, engine.CurrentInitiative);

      foreach ( rendererStruct rend in renderers ) {
        int zPos = Mathf.RoundToInt(gameObject.transform.position.z);
        //zPos /= 3;
        rend.renderer.sortingOrder = ( zPos * -1 ) + rend.sortingOffset;
      }
    }

    /// <summary>
    /// When a character is hit
    /// </summary>
    /// <param name="amount"></param>
    public virtual void Hit(float amount) {
      stat.Health -= ( amount );
    }

    /// <summary>
    /// When a character is healed
    /// </summary>
    /// <param name="amount"></param>
    public virtual void Heal(float amount) {      
      stat.Health += amount;
    }

    /// <summary>
    /// Applies damage over time
    /// </summary>
    /// <param name="initialDamage"></param>
    /// <param name="time"></param>
    /// <param name="tickDamage"></param>
    public virtual void DamageOverTime(float initialDamage, float time, float tickDamage) {
      Hit(initialDamage);
      OverTimeEffects d = new OverTimeEffects(time, tickDamage, "hit");
      effectOverTime.Add(d);
    }

    /// <summary>
    /// Apply heal over time
    /// </summary>
    /// <param name="initialHeal"></param>
    /// <param name="time"></param>
    /// <param name="tickHeal"></param>
    public virtual void HealOverTime(float initialHeal, float time, float tickHeal) {
      Heal(initialHeal);
      OverTimeEffects d = new OverTimeEffects(time, tickHeal, "heal");
      effectOverTime.Add(d);
    }

    /// <summary>
    /// Modify a stat (buff). calls Modstat
    /// </summary>
    /// <param name="statName">The stats to which that apply</param>
    /// <param name="rate">the multiplier to apply</param>
    /// <param name="time">The time it's going to last</param>
    public virtual void BoostStat(string statName, float rate, float time) {
      stat.ModStatRate(statName, rate);
      OverTimeEffects o = new OverTimeEffects(time, 0, statName);
      effectOverTime.Add(o);
    }

    /// <summary>
    /// Modify a stat (debuff). calls Modstat
    /// </summary>
    /// <param name="stat">The stats to which that apply</param>
    /// <param name="rate">the multiplier to apply</param>
    /// <param name="time">The time it's going to last</param>
    public virtual void HinderStat(string statName, float rate, float time) {
      stat.ModStatRate(statName, -rate);
    }

    /// <summary>
    /// Apply an effect on the character. Mainly for effect that actually changes something on the character.
    /// will reduce the time left of the effect and trigger FinishEffect if the timer is done.
    /// </summary>
    protected virtual void applyOverTimeEffect() {
      foreach ( OverTimeEffects o in effectOverTime ) {
        switch ( o.effect ) {
          case "hit":
            Hit(o.amount);
            break;
          case "heal":
            Heal(o.amount);
            break;
          default:
            break;
        }
        if ( o.isDone() )
          FinishEffect(o);
      }
    }

    /// <summary>
    /// FinishEffect removes the effect from the effect list.
    /// </summary>
    /// <param name="o">overtime (time, amount, effet) </param>
    protected virtual void FinishEffect(OverTimeEffects o) {
      if ( !o.effect.Equals("hit") && !o.effect.Equals("heal") )
        stat.ModStatRate(o.effect, 0, true);
      effectOverTime.Remove(o);
    }

    /// <summary>
    /// idDead checks if this character is dead. 
    /// TODO Maybe save the caracter and unInstanciate? or setInactive may be enough?
    /// </summary>
    /// <returns>the state of avliveliness of this character.</returns>
    public virtual bool isDead() {
      bool res = false;
      if ( stat.Health <= 0 ) {
        if ( !isAlreadyDead )
          OnDeath();
        res = true;
      }
      return res;
    }

    /// <summary>
    /// Triggers the death animation.
    /// </summary>
    public void OnDeath() {
      isAlreadyDead = true;
      SceneController.Instance.RemoveTeamMember(this, teamName);
      animator.SetTrigger("death");
    }

  } // end class
}