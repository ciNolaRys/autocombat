﻿using System.Collections.Generic;
using UnityEngine;
using System;
using Game.InventorySystem;
using Game.Data;
using Game.Controller;
using Game.Util;

namespace Game.Characters {
  /// <summary>
  /// This class is used to save character from the editor, and load them into the scene
  /// </summary>
  [Serializable]
  public class CharacterFactory:IDatabase, IContentInstance {
    
    public string Name {
      get {
        return name;
      }
    }    
    public GameObject PrefabObject {
      get {
        return prefabObj;
      }

      set {
        prefabObj = value;
      }
    }
    public string Description { get { return description; } set { description = value; } }
    public string description;

    public EnumPool SlotPool {
      get {
        return EnumPool.CharacterSlot;
      }
    }

    public Sprite Sprite {
      get {
        return avatar;
      }
    }

    public int id;
    public string name;

    public float initiative;
    public float health;
    public float energy;
    
    public string teamName;
    public GameObject prefabObj;
    public Sprite avatar;
    /// <summary>
    /// Contains the ability this character can Use.
    /// </summary>
    public List<int> abilities = new List<int>();

    /// <summary>
    /// for Editor use only. Contains ALL the abilities, and only the one checked are true.
    /// </summary>
    public Dictionary<int,bool> abilityDict = new Dictionary<int, bool>();
    
    // Below are EDITOR ONLY stuff.

#if UNITY_EDITOR
    /// <summary>
    /// Called from the editor, set an ability to available or not for this character.
    /// </summary>
    /// <param name="i">the ability id</param>
    /// <param name="b">is available or not</param>
    public void checkAbility(int i,bool b) {
      if (abilityDict.ContainsKey(i)) {
        abilityDict[i] = b;
      } else {
        abilityDict.Add(i,b);
      }
      setAbilities();
    }
    
    /// <summary>
    /// called from editor, instanciate the ability dictionnary.
    /// </summary>
    public void checkAbility() {
      // on first load, set the Dict.
      if (abilityDict.Count == 0) {
        foreach (int a in abilities) {
          abilityDict.Add(a,true);
        }
      }
    }
    
    /// <summary>
    /// Refresches all abilities for the editor view.
    /// </summary>
    public void setAbilities() {
      abilities.Clear();
      foreach (KeyValuePair<int,bool> a in abilityDict) {
        if (a.Value) {
          abilities.Add(a.Key);
        }
      }
    }
#endif    
    /// <summary>
    /// clone a copy of that ability. 
    /// </summary>
    /// <returns></returns>
    public object GetCopy() {
      return (CharacterFactory)MemberwiseClone();
    }

  }
}