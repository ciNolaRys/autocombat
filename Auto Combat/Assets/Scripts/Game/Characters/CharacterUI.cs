﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

namespace Game.Characters {
  /// <summary>
  /// Class that controls the CharacterUI bar fill.
  /// Maybe some day, i'll transfer all Animation stuff in here too
  /// </summary>
  public class CharacterUI:MonoBehaviour {

    private Slider castingTimerSlider;
    private Slider initiativeTimerSlider;
    private Slider healthBarSlider;
    private Slider healthTransitionSlider;

    /// <summary>
    /// Initialize the labels UI element attached to a character.
    /// </summary>
    public void Start() {
      
      List<Slider> sliders = new List<Slider>(gameObject.GetComponentsInChildren<Slider>());

      castingTimerSlider = sliders.Find(x => x.gameObject.name == "CastingTimer");
      initiativeTimerSlider = sliders.Find(x => x.gameObject.name == "ActionTimer");
      healthBarSlider = sliders.Find(x => x.gameObject.name == "Healthbar");
      healthTransitionSlider = sliders.Find(x => x.gameObject.name == "HealthbarTransition");
    }

    /// <summary>
    /// refresh the value of the Fill based on the % time Left to cast the spell.
    /// </summary>
    public void SetCastingFill(float max,float current) {
      castingTimerSlider.value = current / max;
    }

    /// <summary>
    /// refresh the value of the Fill based on the % time Left for the next action.
    /// </summary>
    public void InitiativeFill(float max,float current) {
      initiativeTimerSlider.value = current / max;
    }

    /// <summary>
    /// the trasition fill bar is the one that slowly come down. it's behind the real health.
    /// It should be used with a LERP effect.
    /// </summary>
    /// <param name="max"></param>
    /// <param name="current"></param>
    public void setHealthTransitionFill(float max,float current) {
      healthTransitionSlider.value = current / max;
    }

    /// <summary>
    /// The real current health.
    /// </summary>
    /// <param name="max"></param>
    /// <param name="current"></param>
    public void SetHealthBarFill(float max,float current) {
      healthBarSlider.value = current / max;
    }

    /// <summary>
    /// Resets the action fill to 0
    /// </summary>
    public void ResetActionTimerFill() {
      initiativeTimerSlider.value = 0;
    }

    /// <summary>
    /// Resets the Casting fill to 0
    /// </summary>
    public void ResetCastingTimerFill() {
      castingTimerSlider.value = 0;
    }

  }
}