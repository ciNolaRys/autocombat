﻿using Game.Spells;
using Game.Characters.Behaviors.Conditions.Enumerations;
using System;

namespace Game.Characters.Behaviors.Conditions {
  [Serializable]
  class ConditionClass:Condition {

    public ConditionClass(ConditionFactory c) : base(c) { }

    public override float Evaluate(CharacterView character,SpellFactory ability) {
      
      bool res = false;
      float value = 0;

      switch (operation) {

        case Operator.Equal:
          res = character.otherCharacterStats.Class.Equals(val);
          break;
        
        case Operator.NotEqual:
          res = !character.otherCharacterStats.Class.Equals(val);
          break;

        default:
          return 0;
      }
      
      if (res)
        value = 1;

      return value;

    }

  }
}
