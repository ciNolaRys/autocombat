﻿using Game.Characters.Behaviors.Conditions.Enumerations;
using System;

namespace Game.Characters.Behaviors.Conditions {

  [Serializable]
  public class ConditionFactory:Data.IDatabase {


    public static readonly string[] operatorStr = {"","lowest","<", "<=" ,"=", ">=", ">", "greatest","!=" };
    public static readonly string[] comparatorStr = {"", "", "%" , "compared to self" };

    public int id;
    public string tag;

    public TargetFilter targetFilter;
    public TargetObject targetObject;
    public Modifier modifier;
    public Operator operation;
    public string val;
    
    public string Name {
      get {
        return
               Enum.GetName(typeof(TargetFilter),targetFilter) + " " +
               Enum.GetName(typeof(TargetObject),targetObject) + " " +
               //Enum.GetName(typeof(EnumOperator),operation) + " " +
               operatorStr[(int)operation] + " " +
               val.ToString() + " " +
               comparatorStr[(int)modifier];
        //Enum.GetName(typeof(EnumComparator),comparator);
      }
    }

    public Guid Id {
      get; private set;
    }

    /// <summary>
    /// public constructor
    /// </summary>
    public ConditionFactory() {
      targetFilter = TargetFilter.None;
      targetObject = TargetObject.None;
      modifier = Modifier.none;
      operation = Operator.none;
      val = "";
      Id = Guid.NewGuid();
    }
    
    public Condition InstaciateCondition() {

      Condition res;

      switch (targetObject) {

        case TargetObject.Target:
          res = new ConditionTarget(this);
          break;

        case TargetObject.Distance:
          res = new ConditionDistance(this);
          break;

        case TargetObject.GroupFactor:
          res = new ConditionGroupFactor(this);
          break;

        case TargetObject.Class:
          res = new ConditionClass(this);
          break;
          
        default:
          res = new ConditionStats(this);
          break;


      }

      return res;

    }

    public object GetCopy() {
      return (ConditionFactory)MemberwiseClone();
    }



  } // end class;
}