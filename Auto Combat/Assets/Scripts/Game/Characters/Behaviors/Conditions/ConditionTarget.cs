﻿using Game.Spells;
using Game.Characters.Behaviors.Conditions.Enumerations;
using Game.Characters;
using System;

namespace Game.Characters.Behaviors.Conditions {
  [Serializable]
  class ConditionTarget:Condition {

    public ConditionTarget(ConditionFactory c) : base(c) { }

    public override float Evaluate(CharacterView character,SpellFactory ability) {
      // TODO : c'est pas exactement le targetValue que je veux ici, mais bien une string quelconque?
      bool res = false;

      switch (operation) {

        case Operator.none:
        case Operator.Lowest:
        case Operator.LowerThan:
        case Operator.LowerOrEqual:
        case Operator.Equal:
          res = character.GetTarget().Equals(val);
          break;
        case Operator.GreaterOrEqual:
        case Operator.GreaterThan:
        case Operator.Greatest:
          break;
        case Operator.NotEqual:
          res = !character.GetTarget().Equals(val);
          break;

      }
      float value = 0;
      if (res)
        value = 1;

      return value;

    }

  }
}
