﻿using Game.Spells;
using Game.Characters;
using Game.Controller;
using System;
using UnityEngine;

namespace Game.Characters.Behaviors.Conditions {
  [Serializable]
  class ConditionGroupFactor:Condition {

    public ConditionGroupFactor(ConditionFactory c) : base(c) { }

    public override float Evaluate(CharacterView character,SpellFactory ability) {
      
      float range = ability.range;
      float distance;
      float value = 0;

      foreach ( Characters.Character c in SceneController.Instance.teams[character.teamName]) {
        distance = Vector3.Distance(c.gameObject.transform.position, character.otherCharacterTransform.position);
        if ( distance <= range) {
          value++;
        }
      }

      if (value >= float.Parse(val))
        return value;
      else
        return 0;

    }

  }
}
