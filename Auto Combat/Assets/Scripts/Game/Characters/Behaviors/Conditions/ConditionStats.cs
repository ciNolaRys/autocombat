﻿using Game.Spells;
using Game.Characters.Behaviors.Conditions.Enumerations;
using System;
using Game.Controller;

namespace Game.Characters.Behaviors.Conditions {
  [Serializable]
  class ConditionStats : Condition, IContentInstance {

    public ConditionStats(ConditionFactory c) : base(c) {}
    
    public override float Evaluate(CharacterView character,SpellFactory ability) {

      bool res = false;

      switch (operation) {

        case Operator.none:
          break;
        case Operator.Lowest:
          break;
        case Operator.LowerThan:
          res = character.GetStat(targetObject) < float.Parse(val);
          break;
        case Operator.LowerOrEqual:
          res = character.GetStat(targetObject) <= float.Parse(val);
          break;
        case Operator.Equal:
          res = character.GetStat(targetObject) == float.Parse(val);
          break;
        case Operator.GreaterOrEqual:
          res = character.GetStat(targetObject) >= float.Parse(val);
          break;
        case Operator.GreaterThan:
          res = character.GetStat(targetObject) > float.Parse(val);
          break;
        case Operator.Greatest:
          break;
        case Operator.NotEqual:
          res = character.GetStat(targetObject) != float.Parse(val);
          break;
      }

      float value = 0;
      if (res)
        value = 1;

      return value;

    }

  }
}
