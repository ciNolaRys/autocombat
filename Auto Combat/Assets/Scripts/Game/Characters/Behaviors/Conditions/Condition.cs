﻿using Game.Characters.Behaviors.Conditions.Enumerations;
using Game.Controller;
using Game.Spells;
using System;
using UnityEngine;
using Game.Util;

namespace Game.Characters.Behaviors.Conditions {

  /// <summary>
  /// This is used by the Behavior system to determine if an Action should be taken.
  /// </summary>
  [Serializable]
  public abstract class Condition:IContentInstance {
    
    public static readonly string[] operatorStr = {"","lowest","<", "<=" ,"=", ">=", ">", "greatest","!=" };
    public static readonly string[] comparatorStr = {"", "", "%" , "compared to self" };

    public int id;
    public string tag;

    public TargetFilter targertFilter;
    public TargetObject targetObject;
    public Modifier modifier;
    public Operator operation;
    public string val;
    private GameObject prefabObject;
    public GameObject PrefabObject {
      get { return prefabObject; }
      set {
        prefabObject = value;
      }
    }
    public bool IsEnemy {
      get { return targertFilter == TargetFilter.Enemy; }
    }
    public bool IsAlly {
      get { return targertFilter == TargetFilter.Ally; }
    }
    public bool IsSelf {
      get { return targertFilter == TargetFilter.Self; }
    }
    public string Name {
      get {
        return
               Enum.GetName(typeof(TargetFilter),targertFilter) + " " +
               Enum.GetName(typeof(TargetObject),targetObject) + " " +
               operatorStr[(int)operation] + " " +
               val.ToString() + " " +
               comparatorStr[(int)modifier];
      }
    }
    public string Description { get { return Name; } }

    public EnumPool SlotPool {
      get {
        return EnumPool.ConditionSlot;
      }
    }
    public Sprite Sprite { get { return null; } }

    /// <summary>
    /// public constructor
    /// </summary>
    public Condition(ConditionFactory c) {
      targertFilter = c.targetFilter;
      targetObject = c.targetObject;
      modifier = c.modifier;
      operation = c.operation;
      val = c.val;
    }

    public abstract float Evaluate(CharacterView character,SpellFactory ability);

    


  } // end class;
}