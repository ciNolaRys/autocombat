﻿using Game.Spells;
using Game.Characters.Behaviors.Conditions.Enumerations;
using Game.Characters;
using System;

namespace Game.Characters.Behaviors.Conditions {
  [Serializable]
  public class ConditionDistance:Condition {

    public ConditionDistance(ConditionFactory c) : base(c) { }

    public override float Evaluate(CharacterView character,SpellFactory ability) {

      float value = 0;

      switch (operation) {

        case Operator.Lowest:
          value = -character.Distance;
          break;
        
        case Operator.Greatest:
          value = character.Distance;
          break;

        default:
          return -1;

      }

      return value;

    }

  }
}
