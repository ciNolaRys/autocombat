﻿namespace Game.Characters.Behaviors.Conditions.Enumerations {
  public enum TargetFilter {

    None=0,

    Self = 100,
    Enemy = 200,
    Ally = 300,
    
  }
}