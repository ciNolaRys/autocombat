﻿namespace Game.Characters.Behaviors.Conditions.Enumerations {
  public enum Operator {
    none = 0,
    Lowest = 1,
    LowerThan = 2,
    LowerOrEqual = 3,
    Equal = 4,
    GreaterOrEqual = 5,
    GreaterThan = 6,
    Greatest = 7,
    NotEqual = 8
  }
}