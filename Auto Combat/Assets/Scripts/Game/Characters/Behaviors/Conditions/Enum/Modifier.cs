﻿namespace Game.Characters.Behaviors.Conditions.Enumerations {
  public enum Modifier {
    none = 0,
    absolute = 1,
    percent = 2,
    comparedToSelf = 3
  }
}