﻿
namespace Game.Characters.Behaviors.Conditions.Enumerations {


  public enum TargetObject {

    None=0,

    Health = 5,
    Mana = 6,

    Agility = 10,
    Strength = 11,
    Stamina = 12,
    Intelligence = 13,
    Cunning = 14,
    Vigor = 15,

    MoveSpeed = 21,
    AttackSpeed = 22,
    AttackPower = 23,
    SpellSpeed = 24,
    SpellPower = 25,
    ArmorRating = 26,

    CharacterLevel = 100,
    Target = 200,
    Distance = 300,
    GroupFactor = 400,
    Class = 500

    
  }



}