﻿using System;
using System.Collections.Generic;

namespace Game.Characters.Behaviors {

  /// <summary>
  /// Contains a Behavior Group. This group is valid only for one Character.
  /// This must be stored in the User for a specific Character. 
  /// </summary>
  [Serializable]
  public class BehaviorGroup:Data.IDatabase{

    public Dictionary<int, Behavior> Behaviors {
      get { return behaviors; }
      set { behaviors = value; }
    }

    public Guid Id {get;private set;}

    private Dictionary<int, Behavior> behaviors = new Dictionary<int, Behavior>();
    private Character character;

    public BehaviorGroup() {
      Id = Guid.NewGuid();
    }

    public BehaviorGroup(Dictionary<int, Behavior> behaviors, Character character) {
      this.behaviors = behaviors;
      this.character = character;
    }

    public object GetCopy() {
      return (BehaviorGroup)MemberwiseClone();
    }
  }
}