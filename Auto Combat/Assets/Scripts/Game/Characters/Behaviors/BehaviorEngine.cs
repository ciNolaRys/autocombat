﻿using Game.Characters.Behaviors.Conditions;
using Game.Characters.Behaviors.Conditions.Enumerations;
using Game.Data;
using Game.Controller;
using Game.Spells;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Game.Controller.InputContexts;
using Game.Util;

namespace Game.Characters.Behaviors {

  public class BehaviorEngine : MonoBehaviour {

    public Character Character { get { return character; } }
    public Cooldown Cooldown {
      get {
        return cooldown;
      }
    }
    public float CurrentInitiative {
      get {
        return initiative[current];
      }
    }
    public float InitialInitiative {
      get {
        return initiative[init];
      }
    }
    public Dictionary<int, Behavior> Behaviors {
      get {
        return behaviors;
      }
    }
    public List<SpellFactory> abilityList = new List<SpellFactory>();

    private Dictionary<int, Behavior> behaviors = new Dictionary<int, Behavior>();
    private Cooldown cooldown;
    private bool isRunning = false;
    private bool isMoving;
    private Character character;
    private float[] initiative = new float[2] { 1f, 1f };
    private int init = 0, current = 1;
    private CharacterView targetCharacter = null;
    private CharacterView previousTarget = null;

    /// <summary>
    /// Struct for the Weight mecanism. This is simply a float,Character association where the float is the weight. 
    /// The added value is the CompareTo IComparable implementation which will sort the list. That way
    /// It's easy to pick the highest Weight value as the winner.
    /// </summary>
    private struct Weight : IComparable<Weight> {
      public float weight;
      public CharacterView character;
      public Weight(float w, CharacterView c) {
        weight = w;
        character = c;
      }

      public int CompareTo(Weight other) {
        if ( other.weight > weight )
          return -1;
        else if ( other.weight == weight )
          return -0;
        else
          return 1;
      }

    }

    void Start() {

      // during dev, 
      DevInit();
      // END dev...
      character = GetComponent<Character>();
      cooldown = gameObject.GetComponent<Cooldown>();

    }

    /// <summary>
    /// Check is the character is dead. This check needs to be here (as opposed to the character class) 
    /// because this Update() method could fire before the Character class. 
    /// Then runs the decision, when not overriden by a User input.
    /// </summary>
    void Update() {


      if ( character.isDead() ) {
        //TODO Destroy this after some time.
        return;
      }
      // todo : set the initiative from the player.
      if ( initiative[current] > 0 ) {
        initiative[current] -= Time.deltaTime;
        return;
      }
      //TODO: check for a User override of the decision making before running the Decisison().
      if ( !isRunning )
        //StartCoroutine(IRunDecision());  //(Not implemented).
        RunDecision();
    }

    public IEnumerator WaitCastingTime(float spellTime, float nextInitiative) {
      initiative[current] = nextInitiative + spellTime;
      while ( spellTime > 0 ) {
        spellTime -= Time.deltaTime;
        yield return null;
      }

    }

    /// <summary>
    /// Executes an action from the Decision Engine.
    /// </summary>
    /// <param name="ability"></param>
    private void ExectuteAction(SpellFactory ability, GameObject targetObject) {

      //GameObject actionObj = Pooler.Instance.GetPooledObject(ability.pool, transform);
      GameObject actionObj = (GameObject) Instantiate(ability.prefabObject, transform);
      actionObj.GetComponent<Spell>().InitSpell(ability, transform, targetObject);
      StartCoroutine(WaitCastingTime(ability.castingTime, ability.nextInitiative));
      cooldown.setCooldown(ability);
    }

    /// <summary>
    /// executed on Update() when the character is ready to act.
    /// </summary>
    /// <returns></returns>
    public CharacterView RunDecision() {
      isRunning = true;
      CharacterView charview = null;

      for ( int i = 0; i < behaviors.Count; i++ ) {

        if ( !behaviors.ContainsKey(i) )
          continue;

        Condition cond = behaviors[i].condition;
        SpellFactory ability = behaviors[i].ability;
        if ( cond == null || ability == null )
          continue;

        if ( !cooldown.isReady(ability.Name) )
          continue;

        charview = EvaluateCondition(behaviors[i]);

        if ( charview != null ) {
          ExectuteAction(ability, charview.otherCharacterTransform.gameObject);
          break;
        }
      }

      isRunning = false;
      return charview;
    }

    public void UpdateBehavior(Dictionary<int, Behavior> newBehavior) {
      behaviors = newBehavior;
    }

    public CharacterView EvaluateCondition(Behavior b) {

      List<Weight> weights = new List<Weight>();
      foreach ( CharacterView cv in character.WorldView.GetTargetCharacters(b.condition.targertFilter) ) {
        weights.Add(new Weight(b.condition.Evaluate(cv, b.ability), cv));
      }
      weights.Sort();

      if ( weights.Count == 0 ) {
        return previousTarget;
      }

      if ( weights.Last().weight == weights.Find(x => x.character == previousTarget).weight )
        return previousTarget;

      if ( weights.Last().weight != 0 )
        return weights.Last().character;

      return null;
    }

    /// <summary>
    /// Loads ability, injected by the game controller
    /// </summary>
    /// <param name="abilities">a list of ability ids.</param>
    public void LoadAbilities(List<int> abilities) {

      // todo change this to get the abilities from the user.character.ability

      AbilityList al = AbilityDatabaseDAO.LoadResource();

      List <SpellFactory> a = al.spellList;
      foreach ( int i in abilities ) {
        abilityList.Add(a.Find(x => x.id == i));
      }
    }

    /// <summary>
    /// Execute an action from the User Input. This should override a decision from the Engine.
    /// </summary>
    /// <param name="context"></param>
    public void ExecuteAction(InputContext context) {
      throw new NotImplementedException();
    }

    private void DevInit() {
      ConditionFactory c = new ConditionFactory();
      SpellFactory a = new SpellFactory();
      ConditionStats cs;
      AbilityList al = AbilityDatabaseDAO.LoadResource();

      c.targetFilter = TargetFilter.Self;
      c.targetObject = TargetObject.Health;
      c.modifier = Modifier.percent;
      c.operation = Operator.LowerOrEqual;
      c.val = "80";      
      a = al.spellList.Find(x => x.id == 1);
      cs = new ConditionStats(c);
      behaviors.Add(0, new Behavior(cs, a));
      abilityList.Add(a);


      c = new ConditionFactory();
      c.targetFilter = TargetFilter.Enemy;
      c.targetObject = TargetObject.Health;
      c.modifier = Modifier.absolute;
      c.operation = Operator.GreaterThan;
      c.val = "0";
      a = al.spellList.Find(x => x.id == 0);
      abilityList.Add(a);
      cs = new ConditionStats(c);
      behaviors.Add(1, new Behavior(cs, a));
    }


    // Stuff below may be fun to implement but it's not working as is... need serious rework;

    //public IEnumerator IRunDecision() {

    //  isRunning = true;
    //  foreach (KeyValuePair<int,Behavior> keyVal in behaviors) {
    //    Condition cond = keyVal.Value.condition;

    //    if (cond.IsConditionStat)
    //      StartCoroutine(IEvaluateStats(cond));

    //    else if (cond.IsConditionDistance)
    //      StartCoroutine(IEvaluateDistance(cond));

    //    else if (cond.IsConditionTarget)
    //      StartCoroutine(IEvaluateTarget(cond));

    //    if (targetCharacter != null) {
    //      //ExectuteAction(keyVal.Value.ability);
    //      isRunning = false;
    //      yield break;
    //    }
    //    yield return null;
    //  }
    //}

    //public IEnumerator IEvaluateDistance(Condition c) {

    //  List<Weight> weights = new List<Weight>();
    //  foreach (CharacterView cv in worldView.GetTargetCharacters(c.targetType)) {
    //    weights.Add(new Weight(cv.Distance,cv));
    //    yield return null;
    //  }
    //  weights.Sort();
    //  if (c.operation == EnumOperator.Greatest)
    //    targetCharacter = weights.Last().character;
    //  else
    //    targetCharacter = weights.First().character;
    //}

    //public IEnumerator IEvaluateStats(Condition c) {

    //  foreach (CharacterView cv in worldView.GetTargetCharacters(c.targetType)) {

    //    float targetStatValue = cv.GetStat(c.conditionTarget);
    //    if (cv.GetStat(c.conditionTarget) >= 0 && c.Evaluate(targetStatValue)) {
    //      targetCharacter = cv;
    //      yield break;
    //    }
    //    yield return null;
    //  }

    //}

    //public IEnumerator IEvaluateTarget(Condition c) {

    //  foreach (CharacterView cv in worldView.GetTargetCharacters(c.targetType)) {
    //    CharacterView cs = cv.GetTarget();
    //    if (cs != null) {
    //      targetCharacter = cs;
    //      yield break;
    //    }
    //    yield return null;
    //  }
    //}

    ///// <summary>
    ///// Executes the action the user inputs.
    ///// </summary>
    ///// <param name="context"></param>
    //public void ExecuteAction(InputContext context) {

    //  GameObject spellObject = Instantiate(context.currentAbility.abilityObject);
    //  spellObject.transform.SetParent(gameObject.transform);
    //  Spell spell;

    //  if (context.hit.point.Equals(new Vector3()))
    //    spell = spellObject.GetComponent<Spell>().InitSpell(context.currentAbility,gameObject.transform);
    //  else
    //    spell = spellObject.GetComponent<Spell>().InitSpell(context.currentAbility,gameObject.transform,context.hit);

    //  StartCoroutine(WaitCastingTime(spell.castingTime,context.currentAbility.nextInitiative));

    //}














  } // class
}