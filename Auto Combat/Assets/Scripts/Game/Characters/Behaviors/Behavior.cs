﻿using Game.Spells;
using Game.Characters.Behaviors.Conditions;

namespace Game.Characters.Behaviors {
  public class Behavior {

    public Condition condition;
    public SpellFactory ability;

    public Behavior(Condition c,SpellFactory a) {
      Init(c,a);
    }
    
    private void Init(Condition c,SpellFactory a) {
      condition = c;
      ability = a;
    }
  }
}