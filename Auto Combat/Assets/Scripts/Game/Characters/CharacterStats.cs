﻿using System;
using System.Reflection;

namespace Game.Characters {
  [Serializable]
  public class CharacterStats {

    // public properties
    public string Class { get; set; }
    public float Health {
      get {
        return health[current];
      }
      set { health[current] = value; }
    }
    public float BaseHealth {
      get {
        return health[init];
      }

    }
    public float ProgressHealth {
      get {
        return health[progress];
      }
      set { health[progress] = value; }
    }

    public float Mana {
      get {
        return mana[current];
      }
      set { mana[current] = value; }
    }
    public float Agility {
      get {
        return agility[current];
      }
      set { agility[current] = value; }
    }
    public float Strength {
      get {
        return strength[current];
      }
      set { strength[current] = value; }
    }
    public float Stamina {
      get {
        return stamina[current];
      }
      set { stamina[current] = value; }
    }
    public float Intelligence {
      get { return intelligence[current]; }
      set { intelligence[current] = value; }
    }
    public float Cunning {
      get { return cunning[current]; }
      set { cunning[current] = value; }
    }
    public float Vigor {
      get {
        return vigor[current];
      }
      set { vigor[current] = value; }
    }

    public float MoveSpeed {
      get {
        return moveSpeed[current];
      }
      set { moveSpeed[current] = value; }
    }
    public float AttackSpeed {
      get {
        return attackSpeed[current];
      }
      set { attackSpeed[current] = value; }
    }
    public float AttackPower {
      get {
        return attackPower[current];
      }
      set { attackPower[current] = value; }
    }
    public float SpellSpeed {
      get {
        return spellSpeed[current];
      }
      set { spellSpeed[current] = value; }
    }
    public float SpellPower {
      get {
        return spellPower[current];
      }
      set { spellPower[current] = value; }
    }
    public float ArmorRating {
      get {
        return armorRating[current];
      }
      set { armorRating[current] = value; }
    }

    // private vars
    private const int init = 0, current = 1, progress = 2;
    private float[] health = {100f,100f,100f };
    private float[] mana = {100f,100f,100f };

    private float[] agility = {0f,0f};
    private float[] strength ={0f,0f};
    private float[] stamina = {0f,0f};
    private float[] intelligence = {0f,0f};
    private float[] cunning = {0f,0f};
    private float[] vigor = {0f,0f};

    private float[] moveSpeed = {0f,0f};
    private float[] attackSpeed = {0f,0f};
    private float[] attackPower = {0f,0f};
    private float[] spellSpeed = {0f,0f};
    private float[] spellPower = {0f,0f};
    private float[] armorRating ={0f,0f};

    // Methods

    /// <summary>
    /// Modify a Stat using reflection.
    /// </summary>
    /// <param name="statName"></param>
    /// <param name="rate"></param>
    /// <param name="reset"></param>
    /// <param name="basestat"></param>
    public void ModStatRate(string statName,float rate,bool reset = false,bool basestat = false) {
      
      var bindings = BindingFlags.IgnoreCase | BindingFlags.NonPublic | BindingFlags.Instance;
      float[] stat = (float[])GetType().GetField(statName,bindings).GetValue(this);

      if (basestat)
        stat[init] *= rate;

      if (reset)
        stat[current] = stat[init];
      else
        stat[current] = stat[current] * rate;
    }
    
    public void ModStatAbsolute(string statName,float rate,bool reset = false,bool basestat = false) {

      var bindings = BindingFlags.IgnoreCase | BindingFlags.NonPublic | BindingFlags.Instance;
      float[] stat = (float[])GetType().GetField(statName,bindings).GetValue(this);

      if (basestat)
        stat[init] += rate;

      if (reset)
        stat[current] = stat[init];
      else
        stat[current] = stat[current] + rate;
    }

    public void Heal (float amount) {
      if ((health[current] + amount) == health[init] ) {

      }
    }


  }
}
