﻿using Game.Characters.Behaviors.Conditions.Enumerations;
using Game.Controller;
using System.Collections.Generic;
using UnityEngine;


namespace Game.Characters {

  public class WorldView : MonoBehaviour {

    public List<CharacterView> Enemies {
      get {
        return allCharacters.FindAll(x => !x.teamName.Equals(thisCharacter.teamName));
      }
    }
    public List<CharacterView> Allies {
      get {
        return allCharacters.FindAll(x => x.teamName.Equals(thisCharacter.teamName));
      }
    }
    public Character ThisCharacter {
      get {
        return thisCharacter;
      }

      set {
        thisCharacter = value;
      }
    }

    private List<CharacterView> allCharacters = new List<CharacterView>();
    private SceneController scene;
    private Character thisCharacter;

    void Start() {
      scene = SceneController.Instance;
      thisCharacter = GetComponent<Character>();
    }

    void Update() {
      // Add/remove any missing characters
      if ( allCharacters.Count != scene.AllCharacters.Count - 1 ) {
        //seeNewCharacter = true;
        foreach ( Character character in scene.AllCharacters ) {
          if ( thisCharacter != character && ( allCharacters.Find(x => x.otherCharacterStats.Equals(character)) ) == null ) {
            allCharacters.Add(new CharacterView(thisCharacter, character));
          }
        }
      }
    }

    void FixedUpdate() {

      RaycastHit hit;

      int layermask = 1 << LayerMask.NameToLayer("character");
      // check the surrounding to see if this Character can see the others.
      foreach ( CharacterView c in allCharacters ) {

        gameObject.GetComponent<BoxCollider>().enabled = false;
        Ray ray = new Ray(thisCharacter.transform.position, c.otherCharacterTransform.position - thisCharacter.transform.position);


        if ( Physics.Raycast(ray, out hit, 10000, layermask) ) {
          GameObject obj = hit.transform.gameObject;
          Character targetChar = obj.GetComponent<Character>();
          if ( targetChar != thisCharacter ) {
            c.isVisisble = true;
            c.Distance = Vector3.Distance(thisCharacter.transform.position, c.otherCharacterTransform.position);
          }
          else {
            c.isVisisble = false;
            c.Distance = -1;
          }
        }
        gameObject.GetComponent<Collider>().enabled = true;

      }
    }

    public List<CharacterView> GetTargetCharacters(TargetFilter c) {

      List<CharacterView> cv = new List<CharacterView>();

      switch ( c ) {
        case TargetFilter.Enemy:
          cv = Enemies;
          return cv;
        case TargetFilter.Ally:
          cv = Allies;
          return cv;
        case TargetFilter.Self:
          cv.Add(new CharacterView(thisCharacter, thisCharacter));
          return cv;
        case TargetFilter.None:
          break;

      }

      return new List<CharacterView>();
    }

  }
}