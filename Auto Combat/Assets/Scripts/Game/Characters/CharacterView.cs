﻿using Game.Characters.Behaviors.Conditions.Enumerations;
using System;
using UnityEngine;

namespace Game.Characters {
  /// <summary>
  /// Class that represents how a character sees another character.
  /// Since this is based on perception, it can be innacurate. 
  /// For example, if a character goes beserk, he can confuse the teamName
  /// or have the stats all mixed up.
  /// The idea was to build the stat sheet over time... we'll see if that makes sense.
  /// 
  /// 1 - get an empty stat object.
  /// 2 - at each interraction with otherCharacter, ThisCharacter would asses its health, mana and other stuff.
  /// To asses Mana, thisCharacter would have to know about arcane though... so it may be inacurate.
  /// Anyway, that's the dream... seems a lot of effort, and i'm unsure of the reward.
  /// You know what they say : Keep it simple stupid.
  /// </summary>
  public class CharacterView {

    public bool isVisisble = false;
    public Transform otherCharacterTransform;
    public CharacterStats otherCharacterStats;
    public float Distance {
      get {
        return distance;
      }
      set {
        distance = value;
      }
    }
    public string teamName;
    
    private GameObject target=null;
    private float distance;
    
    public CharacterView(Character thisCharacter,Character otherCharacter) {
      otherCharacterTransform = otherCharacter.gameObject.transform;
      distance = Vector2.Distance(thisCharacter.transform.position,otherCharacter.transform.position);
      this.otherCharacterStats = otherCharacter.Stat;
      teamName = otherCharacter.teamName;
   
    }

    /// <summary>
    /// gets a statistic for a character using reflexion.
    /// </summary>
    /// <param name="stat">an Enum of type TargetObject corresponding to the stat required.</param>
    /// <returns>the value of that statistic</returns>
    public float GetStat(TargetObject stat) {
      string statName = Enum.GetName(typeof(TargetObject),stat);
      float statValue = (float)otherCharacterStats.GetType().GetProperty(statName).GetValue(otherCharacterStats,null);      
      return statValue;      
    }

    /// <summary>
    /// Returns the Target of this Character, as viewed by another character.
    /// Keep in mind this is a CharacterView, and could contain errors based on the perception of the viewer.
    /// </summary>
    /// <returns>The current target</returns>
    public CharacterView GetTarget() {
      return target.GetComponent<CharacterView>();
    }


  }
}