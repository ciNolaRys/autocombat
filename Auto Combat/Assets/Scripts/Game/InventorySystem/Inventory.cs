﻿using Game.Characters;
using Game.Spells;
using System;
using System.Collections.Generic;

namespace Game.InventorySystem {
  [Serializable]
  public class Inventory {

    public ItemWeapon Weapon { get { return weaponSlot; } }
    public ItemArmor Armor { get { return armorSlot; } }

    private List<InventoryItem> inventory;
    private ItemWeapon weaponSlot;
    private ItemArmor armorSlot;
    private CharacterStats stat;

    public Inventory(CharacterStats s) {
      stat = s;
    }


    public void SetArmor(ItemArmor armor) {
      armorSlot = armor;
      ApplyPassiveEffects(armor);
    }

    public void SetWeapon(ItemWeapon weapon) {
      weaponSlot = weapon;
      ApplyPassiveEffects(weapon);
    }

    public void ApplyPassiveEffects(InventoryItem item) {
      foreach (Spell effect in item.passiveEffects) {
        stat.ModStatRate(effect.TargetStat,effect.Power,true,true);
      }
    }

  }
}
