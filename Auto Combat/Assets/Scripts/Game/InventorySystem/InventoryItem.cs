﻿
using Game.Spells;
using System.Collections.Generic;
using UnityEngine;

namespace Game.InventorySystem {
  public class InventoryItem {

    public Sprite icon;
    public List<Spell> passiveEffects;
    public List<Spell> activeEffects;

  }
}
