﻿using Game.Characters;
using Game.Controller.PanelControllers;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI {
  public class ControlPanelController: MonoBehaviour {
    
    public Character Character { get { return character; } }
    public GameObject characterIcon;
    public PanelControllerBehavior BehaviorController { get { return behaviorPanel; } }

    private Character character;
    private GameObject characterInfoDrawer;
    private GameObject behaviorDrawer;    
    private PanelControllerBehavior behaviorPanel;

    private PanelControllerCondition conditionPanel;
    private PanelControllerAction actionPanel;

    void Start() {
      
      List<Transform> allComponents = new List<Transform>(GetComponentsInChildren<Transform>());

      characterInfoDrawer = allComponents.Find(x => x.gameObject.name.Equals("CharacterInfoDrawer")).gameObject;
      behaviorDrawer = allComponents.Find(x => x.gameObject.name.Equals("BehaviorDrawer")).gameObject;

      behaviorPanel = GetComponentInChildren<PanelControllerBehavior>();
      conditionPanel = GetComponentInChildren<PanelControllerCondition>();
      actionPanel = GetComponentInChildren<PanelControllerAction>();

      characterIcon.SetActive(false);
    }
    

    public void SelectCharacter(Character c) {
      character = c;
      if (c == null) { 
        characterIcon.SetActive(false);

        conditionPanel.ClearUI();
        actionPanel.ClearUI();
        behaviorPanel.ClearUI();
        return;
      }
      characterIcon.SetActive(true);
      Image icon = characterIcon.GetComponentsInChildren<Image>().ToList().Find(x=>x.name.Equals("Image"));      
      icon.sprite = c.avatar;
      
      behaviorPanel.BuildContent(character.Engine);
      conditionPanel.BuildContent(character.Engine);
      actionPanel.BuildContent(character.Engine);
    }

    
  }
}
