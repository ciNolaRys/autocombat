﻿using Game.Data;
using Game.Characters;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Game.Users;
using System;

namespace Game.UI {

  /// <summary>
  /// Class used by the CharacterSelection Screen.
  /// </summary>
  [Obsolete ("replaced by charactyerSelectPanelController")]
  public class SquadBuilder : MonoBehaviour {

    [SerializeField]
    private GameObject characterImage;
    private List<GameObject> itemList = new List<GameObject>();

    public User user;
    
    public void Init() {
      RectTransform[] uiObjects = GetComponentsInChildren<RectTransform>();

      for ( int i = 0; i < uiObjects.Length; i++ ) {
        if ( uiObjects[i].gameObject.name.Equals("AvailableCharacters") )
          BuildAvailableCharacters(uiObjects[i].transform);
      }
    }

    private void BuildAvailableCharacters(Transform parent) {

      CharacterList characterList = CharacterDatabaseDAO.LoadResource();
      List<UserCharacter> userCharacters = user.Characters;

      foreach ( UserCharacter UserCharacter in userCharacters ) {

        CharacterFactory cf = characterList.GetItemById(UserCharacter.CharacterId);
        
        Image[] images = characterImage.GetComponentsInChildren<Image>();
        for ( int i = 0; i < images.Length; i++ ) {
          if ( images[i].gameObject.name.Equals("Image") ) {
            images[i].sprite = cf.avatar;
            break;
          }
        }
        GameObject item = Instantiate(characterImage, parent) as GameObject;
        item.GetComponent<CharacterUIController>().SetCharacter(cf);
        itemList.Add(item);
      }

    }

    
    void OnGUI() {
      for ( int i = 0; i < itemList.Count; i++ ) {
        itemList[i].transform.SetSiblingIndex(i);
      }
    }




  } // class

}
