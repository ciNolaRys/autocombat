﻿using Game.Controller.PanelControllers;

namespace Game.UI {
  class BehaviorDrawer:DrawerController {

    private PanelControllerBehavior BehaviorPannelController;
    private ControlPanelController controlPannel;

    protected override void Init() {
      BehaviorPannelController = GetComponentInChildren<PanelControllerBehavior>();
      controlPannel = GetComponentInParent<ControlPanelController>();
    }

    public override void Switch() {
      base.Switch();
    }

  }
}
