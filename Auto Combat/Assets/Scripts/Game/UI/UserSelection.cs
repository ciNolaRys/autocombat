﻿using Game.Controller;
using Game.Data;
using Game.Users;
using Game.Util;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI {
  class UserSelection : MonoBehaviour {

#pragma warning disable CS0649
    [Tooltip("The container that will receive the userRowPrebafs.")]
    /// <summary>
    /// The container that will receive the userRowPrebafs.
    /// </summary>
    public GameObject userSCContent;
    public GameObject userRowPrebab;
#pragma warning restore CS0649

    void Start() {
      Init();
    }

    /// <summary>
    /// Clears and builds the list of users to display.
    /// </summary>
    private void Init() {
      Utils.DestroyGameObjectChildren(userSCContent);
      UserList userList = UserDatabaseDAO.LoadUserDatabase();

      foreach ( User user in userList.userList ) {
        if ( user.Name.Equals("dummy") )
          continue;

        GameObject row = Pooler.Instance.GetPooledObject(EnumPool.UserRow, userRowPrebab, userSCContent.transform);
        
        SetListener(user, row.GetComponent<Button>());
        row.GetComponentInChildren<Text>().text = user.Name;
      }

      GameObject createNew = (GameObject)Instantiate(userRowPrebab, userSCContent.transform);
      createNew.GetComponentInChildren<Text>().text = "Create new user";
      createNew.GetComponent<Button>().onClick.AddListener(delegate { CreateUser(createNew); });

    }

    public void SetListener(User user, Button button) {
      button.onClick.RemoveAllListeners();
      button.onClick.AddListener(delegate { GameController.Instance.SelectUser(user); });
    }

    /// <summary>
    /// highligh the selected User.
    /// </summary>
    /// <param name="user"></param>
    public void SelectUser(User user) {

      for ( int i = 0; i < userSCContent.transform.childCount; i++ ) {
        userSCContent.transform.GetChild(i).GetComponent<Image>().color = Utils.ConvertColor(51, 51, 51, 255);
        if ( userSCContent.transform.GetChild(i).GetComponentInChildren<Text>().text == user.Name )
          userSCContent.transform.GetChild(i).GetComponent<Image>().color = Utils.ConvertColor(50, 80, 5, 255);
      }

    }

    /// <summary>
    /// Displays a inputField to name the new User, along with a save and Cancel button.
    /// </summary>
    /// <param name="obj">the parent gameobject where the new fields will appear under.</param>
    public void CreateUser(GameObject obj) {
      GameObject createNewPrebaf = (GameObject)Resources.Load("prefabs/UI/MainMenu/CreateNewCharacterPanel");
      RectTransform rect = obj.GetComponent<RectTransform>();
      LayoutElement layout = obj.GetComponent<LayoutElement>();
      GameObject createNewPannel = (GameObject)Instantiate(createNewPrebaf, obj.transform);
      RectTransform rectNewPanel = createNewPannel.GetComponent<RectTransform>();
      layout.ignoreLayout = true;
      rect.offsetMin -= new Vector2(0f, rectNewPanel.rect.height);
      rectNewPanel.anchorMax = Vector2.zero;
      rectNewPanel.anchorMin = Vector2.zero;
      rectNewPanel.pivot = Vector2.zero;
      rectNewPanel.anchoredPosition = Vector2.zero;
      List<Button> buttons = createNewPannel.GetComponentsInChildren<Button>().ToList();
      Button save = buttons.Find(x => x.name == "SaveButton");
      Button cancel = buttons.Find(x => x.name == "CancelButton");

      cancel.onClick.AddListener(delegate { Cancel(obj, createNewPannel); });
      save.onClick.AddListener(delegate { SaveUser(obj, createNewPannel); });

    }

    /// <summary>
    /// Saves a new user into the database
    /// </summary>
    /// <param name="o"></param>
    /// <param name="createNewPannel"></param>
    public void SaveUser(GameObject o, GameObject createNewPannel) {
      GameController.Instance.CreateUser(createNewPannel.GetComponentInChildren<InputField>().text);
      Cancel(o, createNewPannel);
      Init();
    }

    public void Cancel(GameObject o, GameObject createNewPannel) {
      Destroy(createNewPannel);
      o.GetComponent<LayoutElement>().ignoreLayout = false;
    }



  }
}
