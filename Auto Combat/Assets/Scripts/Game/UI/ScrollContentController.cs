﻿using Game.Controller;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI {

  /// <summary>
  /// Listener on the scrollview content so the content size will resize itself base on the content it holds.
  /// </summary>
  public class ScrollContentController : MonoBehaviour {
    
    private VerticalLayoutGroup layout;
    private RectTransform contentRect;
    private string listenerName;
    private int childCount;

    void Start() {
      layout = GetComponent<VerticalLayoutGroup>();
      contentRect = GetComponent<RectTransform>();
      UpdateContentScroll();
      listenerName = "sc-" + gameObject.name;
    }
    
    void LateUpdate() {
      if ( childCount != transform.childCount )
        UpdateContentScroll();
    }
    

    /// <summary>
    /// Updates the content view size. Should be call each time the content changes.
    /// </summary>
    public void UpdateContentScroll() {
      if ( transform.childCount == 0 ) {
        contentRect.offsetMin = new Vector2(0, 0);
        return;
      }

      RectTransform rect = transform.GetChild(0).GetComponent<RectTransform>();      
      float size = (transform.childCount * rect.rect.height) + layout.spacing + layout.padding.bottom + layout.padding.top;      
      contentRect.offsetMin = new Vector2(0, -size);
      
    }
  }



}
