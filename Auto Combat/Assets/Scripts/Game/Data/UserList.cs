﻿using UnityEngine;
using System.Collections.Generic;
using System;
using Game.Users;

namespace Game.Data {

  [Serializable]
  public class UserList  {
    //The scriptableObject where the Item getting stored which you create(ItemDatabase)

    [SerializeField]
    public List<User> userList = new List<User>();


    public UserList() {
      userList.Add(new User("dummy", "password", "email"));
    }

    public User GetItem(string name) {

      for ( int i = 0; i < userList.Count; i++ ) {
        if ( userList[i].Name == name)
          return (User)userList[i].GetCopy();
      }
      
      return null;
    }

    public User getItemById(Guid id) {
      for ( int i = 0; i < userList.Count; i++ ) {
        if ( userList[i].Id == id )
          return (User)userList[i].GetCopy();
      }
      return null;
    }


  }

}