﻿using UnityEngine;
using Game.Users;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace Game.Data {

  /// <summary>
  /// The User Database. Contains a list of Serialized User. This uses the C# serialization and not the Unity Asset resources management. 
  /// </summary>
  public static class UserDatabaseDAO {

    // TODO Find a way to load and save async.

    //The List of all Items
    private static string filepath = "./data/user.dat";

    //creates a new ItemDatabase(new instance)
    public static void CreateUserDatabase() {

      // prevents overriting the file.
      if ( File.Exists(filepath) )
        return;

      BinaryFormatter bf = new BinaryFormatter();
      FileStream file = new FileStream(filepath, FileMode.Create);      
      bf.Serialize(file, new UserList());
      file.Close();

    }


    /// <summary>
    /// Loads all users.
    /// </summary>
    /// <returns>A list of all users.</returns>
    public static UserList LoadUserDatabase() {

      UserList res = new UserList();
      if ( !File.Exists(filepath) )
        CreateUserDatabase();

      using ( FileStream file = new FileStream(filepath, FileMode.Open, FileAccess.Read) ) {
        BinaryFormatter bf = new BinaryFormatter();
        res = (UserList)bf.Deserialize(file);
        return res;
      }
    }

    /// <summary>
    /// Saves all users.
    /// </summary>
    /// <param name="newUser">all the users</param>
    public static void SaveNewUser(User user, UserList userList) {
      // TODO : check if all the previous user are still there before open in Truncate mode.

      if ( !File.Exists(filepath) )
        CreateUserDatabase();
      
      if ( userList.userList.Find(x => x.Id == user.Id) == null ) {
        userList.userList.Add(user);
        using ( FileStream file = new FileStream(filepath, FileMode.Truncate) ) {
          BinaryFormatter bf = new BinaryFormatter();
          bf.Serialize(file, userList);

        }
      }
    }


  }
}