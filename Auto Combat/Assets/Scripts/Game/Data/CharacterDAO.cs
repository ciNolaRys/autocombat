﻿using UnityEngine;
using Game.Characters;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Game.Data {

  public static class CharacterDatabaseDAO {
    //The List of all Items
    public static CharacterList asset;
    private static string path = "Data/CharacterDatabase";


#if UNITY_EDITOR
    private static string assetPath = "Assets/Resources/Data/CharacterDatabase.asset";

    /// <summary>
    /// Creates a CharacterList Database;
    /// </summary>
    /// <returns></returns>
    public static CharacterList CreateCharacterDatabase() {
      asset = ScriptableObject.CreateInstance<CharacterList>();
      AssetDatabase.CreateAsset(asset, assetPath);
      AssetDatabase.SaveAssets();
      asset.characterList.Add(new CharacterFactory());
      return asset;
    }

#pragma warning disable 219
    public static CharacterList LoadCharacterDB() {
      CharacterList characterDatabase = AssetDatabase.LoadAssetAtPath<CharacterList>(assetPath);
      if ( characterDatabase == null ) {
        CreateCharacterDatabase();
        characterDatabase = AssetDatabase.LoadAssetAtPath<CharacterList>(assetPath);
      }
      return characterDatabase;
    }
#endif

    public static CharacterFactory FindCharacterById(int id) {
      CharacterList characterList = (CharacterList)Resources.Load(path);
      return characterList.characterList.Find(x => x.id == id);
    }

    public static CharacterList LoadResource() {
      CharacterList characterList = (CharacterList)Resources.Load(path);
      return characterList;
    }


  }

}