﻿using UnityEngine;
using System.Collections.Generic;
using Game.Spells;

namespace Game.Data {
  [System.Serializable]
  public class AbilityList:ScriptableObject {
    //The scriptableObject where the Item getting stored which you create(ItemDatabase)

    [SerializeField]
    public List<SpellFactory> spellList = new List<SpellFactory>();

    public object GetItemById(int id) {
      for (int i = 0;i < spellList.Count;i++) {
        if (spellList[i].id == id)
          return (SpellFactory)spellList[i].GetCopy();
      }
      return null;
    }

    public SpellFactory getItemByName(string name) {
      for (int i = 0;i < spellList.Count;i++) {
        if (spellList[i].name.ToLower().Equals(name.ToLower()))
          return (SpellFactory)spellList[i].GetCopy();
      }
      return null;
    }

    public List<SpellFactory> GetAbilitiesByTag(string tag) {

      List<SpellFactory> al = new List<SpellFactory>();
      foreach (SpellFactory a in spellList) {
        if (a.tag.Contains(tag))
          al.Add(a);
      }
      return al;
    }

    public bool HasChildren(int id) {
      foreach (SpellFactory a in spellList) {
        if (a.parentId == id)
          return true;
      }
      return false;
    }


  }
}