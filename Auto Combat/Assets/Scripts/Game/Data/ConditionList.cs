﻿using UnityEngine;
using System.Collections.Generic;
using Game.Characters;
using Game.Characters.Behaviors.Conditions;

namespace Game.Data {

  [System.Serializable]
  public class ConditionList:ScriptableObject {
    //The scriptableObject where the Item getting stored which you create(ItemDatabase)

    [SerializeField]
    public List<ConditionFactory> conditions = new List<ConditionFactory>();

    public object GetItemById(int id) {
      for (int i = 0;i < conditions.Count;i++) {
        if (conditions[i].id == id)
          return (ConditionFactory)conditions[i].GetCopy();
      }
      return null;
    }

    public ConditionFactory getItemByName(string name) {
      for (int i = 0;i < conditions.Count;i++) {
        if (conditions[i].Name.ToLower().Equals(name.ToLower()))
          return (ConditionFactory)conditions[i].GetCopy();
      }
      return null;
    }

    public List<ConditionFactory> GetconditionsByTag(string tag) {

      List<ConditionFactory> al = new List<ConditionFactory>();
      foreach (ConditionFactory a in conditions) {
        if (a.tag.Contains(tag))
          al.Add(a);
      }
      return al;
    }

  }

}