﻿using UnityEngine;
using Game.Spells;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Game.Data {
  public static class AbilityDatabaseDAO {
    //The List of all Items
    public static AbilityList asset;
    private static string path = "Data/AbilityDatabase";

#if UNITY_EDITOR
    private static string assetPath = "Assets/Resources/Data/AbilityDatabase.asset";

    /// <summary>
    /// Creates the Database.
    /// </summary>
    /// <returns></returns>
    public static AbilityList createAbilitiesDatabase() {
      
      asset = ScriptableObject.CreateInstance<AbilityList>();
      AssetDatabase.CreateAsset(asset, assetPath);      
      AssetDatabase.SaveAssets();

      asset.spellList.Add(new SpellFactory());
      return asset;
    }



#pragma warning disable 219
    public static AbilityList LoadAbilityDB() {

      AbilityList abilityDatabase = AssetDatabase.LoadAssetAtPath<AbilityList>(assetPath);

      if ( abilityDatabase == null ) {
        createAbilitiesDatabase();
        abilityDatabase = AssetDatabase.LoadAssetAtPath<AbilityList>(assetPath);
      }

      return abilityDatabase;
    }

#endif

    public static AbilityList LoadResource() {
      AbilityList abilityDatabase = (AbilityList)Resources.Load(path);
      return abilityDatabase;
    }


  }
}