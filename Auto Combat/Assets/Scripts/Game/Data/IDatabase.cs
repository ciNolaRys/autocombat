﻿

using System;

namespace Game.Data {
  /// <summary>
  /// Interface for all Database Object. 
  /// </summary>
  public interface IDatabase {
    
    object GetCopy();
  } 
}
