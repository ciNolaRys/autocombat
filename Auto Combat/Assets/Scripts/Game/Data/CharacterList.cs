﻿using UnityEngine;
using System.Collections.Generic;
using Game.Characters;
using System;

namespace Game.Data {

  [Serializable]
  public class CharacterList:ScriptableObject {
    //The scriptableObject where the Item getting stored which you create(ItemDatabase)

    [SerializeField]
    public List<CharacterFactory> characterList = new List<CharacterFactory>();

    public CharacterFactory GetItemById(int id) {
      for (int i = 0;i < characterList.Count;i++) {
        if (characterList[i].id == id)
          return (CharacterFactory)characterList[i].GetCopy();
      }
      return null;
    }

    public CharacterFactory getItemByName(string name) {
      for (int i = 0;i < characterList.Count;i++) {
        if (characterList[i].name.ToLower().Equals(name.ToLower()))
          return (CharacterFactory)characterList[i].GetCopy();
      }
      return null;
    }

  }

}