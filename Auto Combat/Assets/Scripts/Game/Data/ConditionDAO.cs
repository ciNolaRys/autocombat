﻿using UnityEngine;
using Game.Characters.Behaviors.Conditions;
#if UNITY_EDITOR
using UnityEditor;
#endif


namespace Game.Data {
  public static class ConditionDatabaseDAO {
    //The List of all Items
    public static ConditionList asset;
    private static string path = "Data/ConditionDatabase";


#if UNITY_EDITOR

    private static string assetPath = "Assets/Resources/Data/ConditionDatabase.asset";

    /// <summary>
    /// Create a Condition Database.
    /// </summary>
    /// <returns></returns>
    public static ConditionList createAbilitiesDatabase() {
      
      asset = ScriptableObject.CreateInstance<ConditionList>();
      AssetDatabase.CreateAsset(asset,  assetPath);      
      AssetDatabase.SaveAssets();
      asset.conditions.Add(new ConditionFactory());
      return asset;
    }
    
#pragma warning disable 219
    public static ConditionList LoadConditionDB() {

      ConditionList conditionDatabase = AssetDatabase.LoadAssetAtPath<ConditionList>(assetPath);

      if ( conditionDatabase == null) {
        Debug.Log("Creating Condition asset");

        createAbilitiesDatabase();
        conditionDatabase = AssetDatabase.LoadAssetAtPath<ConditionList>(assetPath);
      }

      return conditionDatabase;
    }
#endif


    public static ConditionList LoadResource() {
      ConditionList characterList = (ConditionList)Resources.Load(path);
      return characterList;
    }


  }
}