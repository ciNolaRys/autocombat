﻿using UnityEngine;
using Game.Characters;
using Game.Util;

namespace Game.Spells {
  public class Heal:Spell {
    
    private Animator animator;

    public override Spell InitSpell(SpellFactory a,Transform parent,GameObject target) {

      animator = parent.gameObject.GetComponentInChildren<Animator>();
      
      nextInitiative = a.nextInitiative;
      animator.SetTrigger("heal");
      this.target = target;
      power = a.power;
      base.InitSpell(a,parent,target);

      GameObject skill = initiatorCharacter.GetComponent<Character>().SkillGameObject;
      ParticleSystem ps = skill.GetComponent<ParticleSystem>();
      ps.Play(true);
      
      return this;
    }

    protected override void Execute() {
      base.Execute();      
      target.GetComponent<Character>().Heal(power);
      Pooler.Instance.RecycleObjectAndChildren(gameObject);
    }

  }
}