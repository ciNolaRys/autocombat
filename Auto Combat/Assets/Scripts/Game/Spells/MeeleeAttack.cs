﻿using UnityEngine;
using Game.Characters;
using Game.Util;

namespace Game.Spells {
  public class MeeleeAttack:Spell {
    
    private Animator animator;

    public override Spell InitSpell(SpellFactory a,Transform parent,GameObject target) {
      
      animator = parent.gameObject.GetComponentInChildren<Animator>();
      nextInitiative = a.nextInitiative;
      animator.SetTrigger("attack");
      this.target = target;
      power = a.power;
      base.InitSpell(a,parent,target);
      return this;
      
    }
    
    protected override void Execute() {
      base.Execute();
      target.GetComponent<Character>().Hit(power);
      Pooler.Instance.RecycleObjectAndChildren(gameObject);
    }

  }
}