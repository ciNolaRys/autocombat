﻿using Game.Controller;
using Game.Data;
using System;
using System.Collections.Generic;
using UnityEngine;
using Game.Util;

namespace Game.Spells {
  /// <summary>
  /// This class is the base class to hold abilities. it's only used in the editor 
  /// with the ability editor extention.
  /// Ah well.. I renamed this Factory, but it's not quite right.... to bad...
  /// </summary>
  [Serializable]
  public class SpellFactory : IDatabase, IContentInstance
  {

    public string text;
    public int id;
    public string name;
    public Sprite icon;
    public float range;
    public float speed;
    public string targetStat;     // for buffs and debuffs
    public float power;           // how strong is the base effect.
    public float areaOfEffet;     // how far from the center this takes effect.
    public float force;           // controls how knockback effects.
    public int parentId;          // Parent ability for the hierarchy.
    public GameObject prefabObject;  // prefab containing the object of the ability.
    public UnityEditor.MonoScript spell;
    public float castingTime;     // time required before the action takes place.
    public float animationTime;   // time for the animation on the Character to play. This is typically play
    [Obsolete("This is now calculated.", true)]
    public float timeToLive;      // failsafe to destroy the object after its useful life.
    public float nextInitiative = 1.2f;
    public float cooldown;
    public LayerMask targetLayer;

    [Obsolete("This is not used now... Maybe one day, i'll need this again for spell visual effects...")]
    public GameObject PrefabObject {
      get { return prefabObject; }
      set {
        prefabObject = value;
      }
    }
    public string Description { get { return text; } }
    public string Name {
      get { return name; }
    }    
    public Guid Id {
      get; private set;
    }

    public EnumPool pool { get { return EnumPool.Spell; } }
    public EnumPool SlotPool {
      get {
        return EnumPool.ActionSlot;
      }
    }
    public Sprite Sprite { get { return icon; } }

    public List<string> tag = new List<string>(); // a List of all tags related to that ability.

    public SpellFactory() {
      id = 0;
      name = "New Ability";
      Init();
    }

    public SpellFactory(int id, string name, int level, Sprite Icon, float range, int parentId) {
      this.id = id;
      this.name = name;
      icon = Icon;
      this.range = range;
      this.parentId = parentId;
      Init();
    }

    private void Init() {
      Id = Guid.NewGuid();
    }
    
    /// <summary>
    /// clone a copy of that ability. 
    /// </summary>
    /// <returns></returns>
    public object GetCopy() {
      return (SpellFactory)MemberwiseClone();
    }

  }
}