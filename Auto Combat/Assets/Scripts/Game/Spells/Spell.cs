﻿using Game.Characters;
using Game.Util;
using System;
using System.Collections;
using UnityEngine;

namespace Game.Spells {

  /// <summary>
  /// This Is the class that triggers the Action. 
  /// Starts by spawing a new object containing the ability at the startPointTransform of the initiatorCharacter.
  /// InitSpell does the initial stuff. 
  /// The base class update casting bars on the CharacterUI mostly.
  /// </summary>
  public class Spell:MonoBehaviour {

    public string TargetStat { get { return targetStat; } }
    public float Power { get { return power; } }

    public float nextInitiative;
    public float castingTime;
    public float TimeToLive { get { return timeToLive; } }

    protected float castingTimer;
    protected float animationTimer;
    protected float animationTime;
    protected string targetStat;
    protected float power;

    protected GameObject initiatorCharacter;
    protected Transform startPointTransform;
    protected GameObject target;
    protected float timeToLive;
    protected CharacterUI timeFiller;
    protected SpellFactory ability;
    protected Rigidbody thisSpellRigidBody;



    /// <summary>
    /// initialize a spell with a Raycast input (mouse input).
    /// </summary>
    public virtual Spell InitSpell(SpellFactory a,Transform parent,RaycastHit hit) {
      Initializer(a,parent);
      return this;
    }
    
    /// <summary>
    /// initialize a spell with a Raycast input (mouse input).
    /// </summary>
    public virtual Spell InitSpell(SpellFactory a,Transform parent,GameObject target) {
      Initializer(a,parent);
      return this;
    }
    
    /// <summary>
    /// initialize a spell without Raycast (no mouse inputs).
    /// </summary>
    public virtual Spell InitSpell(SpellFactory a,Transform parent) {
      Initializer(a,parent);
      return this;
    }

    private void Initializer(SpellFactory a,Transform parent) {      
      Character character = parent.gameObject.GetComponent<Character>();
      
      ability = a;
      castingTime = a.castingTime;
      animationTime = a.animationTime;
      animationTimer = 0;

      timeFiller = parent.GetComponentInParent<CharacterUI>();
      initiatorCharacter = parent.gameObject;
      thisSpellRigidBody = GetComponent<Rigidbody>();
      timeToLive = a.animationTime + a.castingTime + 0.1f;
      
      startPointTransform = character.ActionPoint.transform;
      transform.SetParent(startPointTransform);
      transform.position = startPointTransform.position;      
      StartCoroutine(StartCasting());
      PostInitSpell();
    }
    
    protected virtual void PostInitSpell() {
      Destroy(gameObject, timeToLive);
    }
    
    [Obsolete ("Find a reference to the charactersheet and you'll get the action point from there;")]
    protected virtual Transform GetSpellTransform() {

      Transform[] t = initiatorCharacter.GetComponentsInChildren<Transform>();

      for ( int i = 0; i < t.Length; i++) {
        if (t[i].gameObject.name.Equals("ActionPoint"))
          return t[i];
      }
      return null;
    }

    /// <summary>
    /// Corouting waiting for the animationTime to finish before sending the Execute Command.
    /// This should be call directly after the the Animator has been set in motion.
    /// </summary>
    /// <returns></returns>
    protected virtual IEnumerator StartAnimation() {
      while (animationTimer <= animationTime) {
        animationTimer += Time.deltaTime;
        yield return null;
      }
      Execute();
    }

    protected virtual IEnumerator StartCasting() {
      while (castingTimer <= castingTime) {
        castingTimer += Time.deltaTime;
        //timeFiller.SetCastingFill(castingTime,animationTimer);
        yield return null;
      }
      StartCoroutine(StartAnimation());
    }
    
    /// <summary>
    /// Cast the actual spell.
    /// </summary>
    protected virtual void Execute() {
      transform.position = startPointTransform.position;
      gameObject.transform.SetParent(null);
      timeFiller.ResetCastingTimerFill();
    }

  }
}