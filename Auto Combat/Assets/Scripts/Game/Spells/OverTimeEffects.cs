﻿using UnityEngine;

namespace Game.Spells {
  public class OverTimeEffects {
    // I think this should be a class someday.
    public float time;
    public float amount;
    public string effect;

    public OverTimeEffects(float t,float a,string e) {
      time = t;
      amount = a;
      effect = e;
    }

    public bool isDone() {
      time -= Time.deltaTime;
      return (time <= 0);
    }
  }
}