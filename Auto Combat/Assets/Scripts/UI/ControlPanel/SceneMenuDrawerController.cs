﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Game.UI {
  class SceneMenuDrawerController:DrawerController {
    private float threshold = 1;
    protected override void Init() {
      RectTransform rect = GetComponent<RectTransform>();
      closedPosision = new Vector2(-250f, 0f);
      currentPosition = closedPosision;
      openedPosision = Vector3.zero;
      nextPosition = openedPosision; 
    }




    public override void Switch() {
      if ( currentPosition == closedPosision )
        nextPosition = openedPosision;
      else
        nextPosition = closedPosision;

      isMoving = true;
    }


    protected override void AnimateBehaviorDrawer() {
      RectTransform drawer = gameObject.GetComponent<RectTransform>();
      
      Vector2 nextStep = Vector2.Lerp(currentPosition, nextPosition, Time.deltaTime * animSpeed);
      Vector2 step = nextStep - currentPosition;
      currentPosition = nextStep;
      
      drawer.Translate(step);
      if ( Vector2.Distance(currentPosition, nextPosition) < threshold ) {
        isMoving = false;
        currentPosition = nextPosition;
        drawer.anchoredPosition = currentPosition;        
      }
    }
    
  }
}
