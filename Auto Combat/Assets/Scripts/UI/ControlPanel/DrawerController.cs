﻿using UnityEngine;
using System.Collections.Generic;


namespace Game.UI {
  /// <summary>
  /// Controls how the window moves.
  /// </summary>
  class DrawerController:MonoBehaviour {

    public float animSpeed = 5f;


    protected Vector2 closedPosision;
    protected Vector2 openedPosision;
    protected Vector2 nextPosition;
    protected Vector2 currentPosition;
    protected bool isMoving;

    void Start() {
      Init();
    }

    protected virtual void Init() {
      RectTransform rect = GetComponent<RectTransform>();
      closedPosision = rect.offsetMax;      
      nextPosition = closedPosision;
      openedPosision = rect.offsetMax + new Vector2(0f,350f);
      currentPosition = openedPosision;
    }

    public virtual void Switch() {
      if (nextPosition == closedPosision)
        nextPosition = openedPosision;
      else
        nextPosition = closedPosision;

      isMoving = true;
    }

    void LateUpdate() {
      if (isMoving)
        AnimateBehaviorDrawer();
    }

    protected virtual void AnimateBehaviorDrawer() {
      RectTransform drawer = gameObject.GetComponent<RectTransform>();

      drawer.offsetMax = Vector2.Lerp(drawer.offsetMax,nextPosition,Time.deltaTime * animSpeed);
      if (Vector2.Distance(drawer.offsetMax,nextPosition) < 0.1) {
        isMoving = false;
        drawer.offsetMax = nextPosition;
      }
    }
    
  }
}
