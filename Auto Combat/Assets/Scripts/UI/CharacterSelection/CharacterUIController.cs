﻿using Game.Characters;
using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Game.UI {
  /// <summary>
  /// Class that controls how a character icon can be interracted with. I think this is obsolete.
  /// </summary>
  [Obsolete("This is not used anymore.")]
  public class CharacterUIController:MonoBehaviour, IDragHandler, IPointerDownHandler, IEndDragHandler {

    private  RectTransform draggingRectTransform;

    private Vector2 pointerOffset;
    private RectTransform rectTransform;
    
    private CanvasGroup canvasGroup;
    private GameObject oldSlot;
    private GameObject draggedCharacter;
    private CharacterFactory character;

    void Start() {
      init();
    }

    private void init() {
      draggingRectTransform = GameObject.FindGameObjectWithTag("DraggingItem").GetComponent<RectTransform>();
      rectTransform = GetComponent<RectTransform>();
      canvasGroup = GetComponent<CanvasGroup>();
    }

    public void SetCharacter(CharacterFactory character) {
      this.character = character;
    }

    public void OnDrag(PointerEventData data) {
      if (rectTransform == null)
        return;

      if (data.button == PointerEventData.InputButton.Left) {
        rectTransform.SetAsLastSibling();
        Vector2 localPointerPosition;
        if (RectTransformUtility.ScreenPointToLocalPointInRectangle(draggingRectTransform,Input.mousePosition,data.pressEventCamera,out localPointerPosition)) {
          rectTransform.localPosition = localPointerPosition - pointerOffset;
        }
      }
    }

    public void OnPointerDown(PointerEventData data) {
      if (data.button == PointerEventData.InputButton.Left) {
        GameObject item = Instantiate(gameObject,draggingRectTransform.transform) as GameObject;

        rectTransform = item.GetComponent<RectTransform>();
        rectTransform.position = data.position;
        draggedCharacter = item;
        draggedCharacter.GetComponent<CanvasGroup>().blocksRaycasts = false;

        RectTransformUtility.ScreenPointToLocalPointInRectangle(rectTransform,data.position,data.pressEventCamera,out pointerOffset);
        oldSlot = transform.parent.gameObject;
      }
    }

    public void OnEndDrag(PointerEventData data) {

      if (data.button == PointerEventData.InputButton.Left) {
        canvasGroup.blocksRaycasts = true;
        Transform newSlot = null;
        string slotName = data.pointerEnter.transform.gameObject.name;
        if (data.pointerEnter != null && data.pointerEnter.transform.parent.gameObject.name.Equals("SelectedCharacters")) {
          newSlot = data.pointerEnter.transform;
        } else if (data.pointerEnter.transform.gameObject.name.StartsWith("CharacterImage") && !data.pointerEnter.transform.parent.gameObject.name.Equals("AvailableCharacters")) {
          newSlot = data.pointerEnter.transform.parent.transform;
          Destroy(data.pointerEnter.transform.gameObject);
        }

        // we found a suitable slot where to put the item

        if (newSlot != null) {
          GameObject item = Instantiate(gameObject,newSlot) as GameObject;
          item.transform.SetAsLastSibling();
          item.GetComponent<RectTransform>().localPosition = Vector3.zero;
        } 
        Destroy(draggedCharacter);
      }



    }

  }
}