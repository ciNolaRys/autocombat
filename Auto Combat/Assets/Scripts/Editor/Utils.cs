﻿using UnityEngine;
using System.Collections.Generic;
using UnityEditor;


namespace Game.Editor {
  public class Utils:EditorWindow {

    /// <summary>
    /// Apply and Cancel buttons Generic
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="bufferedObject">Object that with the new Item</param>
    /// <param name="savedObject">List of object to be saved.</param>
    /// <param name="showNew">out param for foldout</param>
    public static bool DisplayNewObjectButtons<T>(T bufferedObject,List<T> savedObject,ref bool showNew)
        where T : Data.IDatabase {
      bool res = false;
      // Apply Cancel buttons for existing Characters.
      GUILayout.Space(10);
      EditorGUILayout.BeginHorizontal();
      GUILayout.FlexibleSpace();
      if (GUILayout.Button("Apply",GUILayout.Width(100))) {
        savedObject.Add(bufferedObject);
        showNew = false;
        res = true;
      }
      if (GUILayout.Button("Cancel",GUILayout.Width(100))) {
        showNew = false;
        //abilityToSave = null;
      }
      EditorGUILayout.EndHorizontal();
      return res;
    }


    /// <summary>
    /// Undo, save and delete buttons generic. Data passed here need to implement IDatabase<T>
    /// </summary>
    /// <typeparam name="T">Type of data that will get saved.</typeparam>
    /// <param name="position"></param>
    /// <param name="bufferedObject"></param>
    /// <param name="savedObject"></param>
    public static bool DisplayButtons<T>(int position,List<T> bufferedObject,List<T> savedObject)
        where T : Data.IDatabase {
      // UNDO, SAVE and DELETE buttons for existing Characters.
      bool res = false;
      GUILayout.Space(10);
      EditorGUILayout.BeginHorizontal();
      GUILayout.FlexibleSpace();

      if (GUILayout.Button("Undo",GUI.skin.button,GUILayout.Width(100))) {
        bufferedObject[position] = (T)savedObject[position].GetCopy();
        res = true;
      }
      if (GUILayout.Button("Apply",GUILayout.Width(100))) {
        savedObject[position] = (T)bufferedObject[position].GetCopy();
        res = true;
      }
      if (GUILayout.Button("Delete",GUILayout.Width(100))) {
        savedObject.RemoveAt(position);
        bufferedObject.RemoveAt(position);
        res = true;
      }

      EditorGUILayout.EndHorizontal();

      return res;
    }


  }

}