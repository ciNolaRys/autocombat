﻿using Game.Spells;
using Game.Data;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;


namespace Game.Editor {
  public class AbilityEditor:EditorWindow {

    // private Utils utils = new Utils();
    private static AbilityList abilityDB = null;
    private SpellFactory abilityToSave = null;
    private static List<SpellFactory> bufferedAbilitiesToSave = new List<SpellFactory>();
    List<bool> openedAbility = new List<bool>();
    bool showNewAbility=false;
    Vector2 scrollPosition;
    
    [MenuItem("AutoCombat/Abilities")]

    static void Init() {
      GetWindow(typeof(AbilityEditor));
      abilityDB = AbilityDatabaseDAO.LoadAbilityDB();
      ReloadAbilities();
    }

    void OnGUI() {
      DisplayAbilityDatabase();
    }

    /// <summary>
    /// Reload all ability from disk.
    /// </summary>
    static private void ReloadAbilities() {
      bufferedAbilitiesToSave.Clear();
      foreach (SpellFactory a in abilityDB.spellList) {
        bufferedAbilitiesToSave.Add((SpellFactory)a.GetCopy());
      }
    }

    /// <summary>
    /// AbilityDatabase provides a nice Input UI to create / edit Ability. supports UNDO, SAVE, DELETE
    /// </summary>
    private void DisplayAbilityDatabase() {

      if (abilityDB == null) {
        abilityDB = AbilityDatabaseDAO.LoadAbilityDB();
      }
      if (bufferedAbilitiesToSave == null || bufferedAbilitiesToSave.Count == 0)
        ReloadAbilities();

      EditorGUILayout.BeginVertical("Box",GUILayout.Width(position.width - 10));
      scrollPosition = EditorGUILayout.BeginScrollView(scrollPosition);

      EditorGUILayout.BeginHorizontal();
      GUILayout.Label("Found " + bufferedAbilitiesToSave.Count + " abilities in the Database");
      GUILayout.FlexibleSpace();
      GUI.color = Color.white;
      if (GUILayout.Button("Create a new Ability",GUILayout.Width(150))) {
        showNewAbility = true;
        abilityToSave = new SpellFactory();
      }
      if (GUILayout.Button("Save to Disk",GUILayout.Width(150))) {
        EditorUtility.SetDirty(abilityDB);
        AssetDatabase.SaveAssets();
      }
      EditorGUILayout.EndHorizontal();
      GUILayout.Space(10);
      // Ability Creation.
      if (showNewAbility) {

        EditorGUILayout.Foldout(true,abilityToSave.name);
        EditorGUILayout.BeginVertical("Box",GUILayout.Width(position.width - 20));
        abilityToSave.id = abilityDB.spellList.Count + 1;
        abilityToSave = AbilityLayout(abilityToSave);



        if (Utils.DisplayNewObjectButtons(abilityToSave,abilityDB.spellList,ref showNewAbility))
          ReloadAbilities();
        EditorGUILayout.EndVertical();
      }

      for (int i = 0;i < bufferedAbilitiesToSave.Count;i++) {
        if (openedAbility.Count < bufferedAbilitiesToSave.Count)
          openedAbility.Add(false);

        openedAbility[i] = EditorGUILayout.Foldout(openedAbility[i],"" + bufferedAbilitiesToSave[i].name);
        if (openedAbility[i]) {
          EditorGUILayout.BeginVertical("Box",GUILayout.Width(position.width - 20));
          AbilityLayout(bufferedAbilitiesToSave[i]);
          if (Utils.DisplayButtons(i,bufferedAbilitiesToSave,abilityDB.spellList)) {
            openedAbility[i] = false;
          }
          EditorGUILayout.EndVertical();
        }
      }

      EditorGUILayout.EndScrollView();
      EditorGUILayout.EndVertical();
    }

    private SpellFactory AbilityLayout(SpellFactory a) {

      GUILayout.BeginHorizontal();
      GUILayout.Label("Abilities ID :                  ");
      GUILayout.Label("" + a.id);
      GUILayout.FlexibleSpace();
      GUILayout.EndHorizontal();

      GUILayout.BeginVertical();

      a.name = EditorGUILayout.TextField
          ("Abillity Name",a.name,GUILayout.Width(500));
      a.parentId = EditorGUILayout.IntField
          ("Parent Ability",a.parentId,GUILayout.Width(500));
      a.icon = (Sprite)EditorGUILayout.ObjectField
          ("Abillity Icon",a.icon,typeof(Sprite),false,GUILayout.Width(215));

      a.range = EditorGUILayout.FloatField("Abillity Range",a.range,GUILayout.Width(500));
      a.speed = EditorGUILayout.Slider("Speed",a.speed,0,1000,GUILayout.Width(500));
      a.castingTime = EditorGUILayout.Slider ("Casting Time",a.castingTime,0,100,GUILayout.Width(500));
      a.animationTime = EditorGUILayout.Slider("Animation Time",a.animationTime,0, 100, GUILayout.Width(500));
      //a.timeToLive = EditorGUILayout.Slider("Time to live",a.timeToLive,0, 100, GUILayout.Width(500));
      a.nextInitiative = EditorGUILayout.Slider("Next initiative",a.nextInitiative,0, 100, GUILayout.Width(500));
      a.cooldown = EditorGUILayout.Slider("Cooldown",a.cooldown,0, 100, GUILayout.Width(500));
      a.power = EditorGUILayout.Slider("Power",a.power,0, 100, GUILayout.Width(500));

      a.prefabObject = (GameObject) EditorGUILayout.ObjectField ("Spell", a.prefabObject, typeof(GameObject),false);


      GUILayout.Label("Ability Info");
      a.text = EditorGUILayout.TextArea(a.text, GUILayout.Width(500), GUILayout.Height(50));


      GUILayout.EndVertical(); 
      return a;
    }

  }

}