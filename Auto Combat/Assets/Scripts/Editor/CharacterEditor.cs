﻿using Game.Spells;
using Game.Characters;
using Game.Data;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Game.Editor {
  public class CharacterEditor:EditorWindow {

    //private Utils utils = new Utils();
    private static CharacterList characterDB = null;
    private CharacterFactory characterToSave = null;
    private static List<CharacterFactory> bufferedCharacterToSave = new List<CharacterFactory>();
    List<bool> openedCharacter = new List<bool>();
    Dictionary<int,bool> openedAbilities = new Dictionary<int,bool>();
    bool showNewCharacter=false;
    Vector2 scrollPosition;
    Vector2 abilityScrollPosition;

    [MenuItem("AutoCombat/Characters")]

    static void Init() {
      GetWindow(typeof(CharacterEditor));
      characterDB = CharacterDatabaseDAO.LoadCharacterDB();
    }

    void OnGUI() {
      DisplayCharacterDatabase();
    }

    /// <summary>
    /// RefreshBufferedCharacters make sure that the database and the data show on the screen is the same.
    /// </summary>
    static void ReloadCharacters() {
      bufferedCharacterToSave.Clear();
      foreach (CharacterFactory cs in characterDB.characterList) {
        bufferedCharacterToSave.Add((CharacterFactory)cs.GetCopy());
      }
    }

    /// <summary>
    /// DisplayCharacterDatabase provides a nice Input UI to create / edit Characters. supports UNDO, SAVE, DELETE
    /// </summary>
    private void DisplayCharacterDatabase() {

      if (characterDB == null) {
        characterDB = CharacterDatabaseDAO.LoadCharacterDB();
      }
      if (bufferedCharacterToSave == null || bufferedCharacterToSave.Count == 0)
        ReloadCharacters();

      EditorGUILayout.BeginVertical("Box",GUILayout.Width(position.width));
      scrollPosition = EditorGUILayout.BeginScrollView(scrollPosition);
      EditorGUILayout.BeginHorizontal();
      GUILayout.Label("Found " + bufferedCharacterToSave.Count + " character(s) in the Database");
      GUILayout.FlexibleSpace();
      GUI.color = Color.white;
      if (GUILayout.Button("Create a new Character",GUILayout.Width(150))) {
        showNewCharacter = true;
        characterToSave = new CharacterFactory();
      }
      if (GUILayout.Button("Save to Disk",GUILayout.Width(150))) {
        EditorUtility.SetDirty(characterDB);
        AssetDatabase.SaveAssets();
      }
      EditorGUILayout.EndHorizontal();
      GUILayout.Space(10);
      if (showNewCharacter) {
        // NEW Character 
        EditorGUILayout.Foldout(true,characterToSave.name);
        EditorGUILayout.BeginVertical("Box",GUILayout.Width(position.width - 20));
        characterToSave.id = characterDB.characterList.Count + 1;
        characterToSave = CharacterLayout(characterToSave);
        if (Utils.DisplayNewObjectButtons(characterToSave,characterDB.characterList,ref showNewCharacter))
          ReloadCharacters();
        EditorGUILayout.EndVertical();
      }

      // List of Characters
      for (int i = 0;i < bufferedCharacterToSave.Count;i++) {

        if (openedCharacter.Count < bufferedCharacterToSave.Count)
          openedCharacter.Add(false);

        openedCharacter[i] = EditorGUILayout.Foldout(openedCharacter[i],"" + "ID: " + bufferedCharacterToSave[i].id + " | CharacterName : " + bufferedCharacterToSave[i].name);
        if (openedCharacter[i]) {
          EditorGUILayout.BeginVertical("Box",GUILayout.Width(position.width - 20));
          CharacterLayout(bufferedCharacterToSave[i]);
          if (Utils.DisplayButtons(i,bufferedCharacterToSave,characterDB.characterList)) {
            openedCharacter[i] = false;
          }
          EditorGUILayout.EndVertical();
        }
      }
      EditorGUILayout.EndScrollView();
      EditorGUILayout.EndVertical();
    }

    /// <summary>
    /// CharacterLayout governs how the Character UI is shown.
    /// </summary>
    /// <param name="c">Character to save</param>
    /// <returns></returns>
    private CharacterFactory CharacterLayout(CharacterFactory c) {

      GUILayout.BeginHorizontal();
      GUILayout.Label("Character ID :               ");
      GUILayout.Label("" + c.id);
      GUILayout.FlexibleSpace();
      GUILayout.EndHorizontal();

      if (!openedAbilities.ContainsKey(c.id)) {
        openedAbilities.Add(c.id,false);
      }
      c.name = EditorGUILayout.TextField("Character Name : ",c.name,GUILayout.Width(500));

      c.energy = EditorGUILayout.FloatField("Base Energy : ",c.energy,GUILayout.Width(500));
      c.health = EditorGUILayout.FloatField("Base Health : ",c.health,GUILayout.Width(500));
      c.initiative = EditorGUILayout.FloatField("Base Initiative : ",c.initiative,GUILayout.Width(500));
      c.PrefabObject = (GameObject)EditorGUILayout.ObjectField("Character Prefab",c.PrefabObject,typeof(GameObject),true,GUILayout.Width(500));
      c.avatar = (Sprite)EditorGUILayout.ObjectField("Character Avatar",c.avatar,typeof(Sprite),true,GUILayout.Width(500));
      GUILayout.Label("Character Bio");
      c.description = EditorGUILayout.TextArea(c.description, GUILayout.Width(500),GUILayout.Height(50));

      // Ability list.
      AbilityList abilityList = AbilityDatabaseDAO.LoadAbilityDB();

      openedAbilities[c.id] = EditorGUILayout.Foldout(openedAbilities[c.id],"Abilities");
      if (openedAbilities[c.id]) {

        c.checkAbility();

        GUILayoutOption[] options = { GUILayout.MaxHeight(100),GUILayout.MinHeight(60) };

        EditorGUILayout.BeginVertical("Box",options);
        abilityScrollPosition = EditorGUILayout.BeginScrollView(abilityScrollPosition);
        int i = 0;
        EditorGUILayout.BeginHorizontal(GUILayout.Width(600));

        bool needCloseVertical = false;
        foreach (SpellFactory a in abilityList.spellList) {
          if (i % 3 == 0) {
            EditorGUILayout.BeginVertical();
            needCloseVertical = true;
          }

          bool isEnabled = false;
          if (!c.abilityDict.TryGetValue(a.id,out isEnabled)) {
            isEnabled = false;
          }
          c.checkAbility(a.id,GUILayout.Toggle(isEnabled,new GUIContent(a.name),GUILayout.Width(150)));
          i++;
          if (i % 3 == 0) {
            EditorGUILayout.EndVertical();
            needCloseVertical = false;
          }

        }
        if (needCloseVertical)
          EditorGUILayout.EndVertical();
        EditorGUILayout.EndHorizontal();


        EditorGUILayout.EndScrollView();

        EditorGUILayout.EndVertical();
      }

      return c;
    }
  }

}