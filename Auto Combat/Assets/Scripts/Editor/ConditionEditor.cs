﻿using Game.Characters.Behaviors.Conditions;
using Game.Characters.Behaviors.Conditions.Enumerations;
using Game.Data;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Game.Editor {
  public class ConditionEditor:EditorWindow {

    // private Utils utils = new Utils();
    private static ConditionList ConditionDB = null;
    private ConditionFactory ConditionToSave = null;
    private static List<ConditionFactory> bufferedAbilitiesToSave = new List<ConditionFactory>();
    List<bool> openedCondition = new List<bool>();
    bool showNewCondition=false;
    Vector2 scrollPosition;
    [MenuItem("AutoCombat/Conditions")]

    static void Init() {
      GetWindow(typeof(ConditionEditor));
      ConditionDB = ConditionDatabaseDAO.LoadConditionDB();
      ReloadConditions();
    }

    void OnGUI() {
      DisplayConditionDatabase(); 
    }

    /// <summary>
    /// Reload all ConditionSO from disk.
    /// </summary>
    static private void ReloadConditions() {
      bufferedAbilitiesToSave.Clear();
      foreach (ConditionFactory a in ConditionDB.conditions) {
        bufferedAbilitiesToSave.Add((ConditionFactory)a.GetCopy());
      }
    }

    /// <summary>
    /// ConditionDatabase provides a nice Input UI to create / edit ConditionSO. supports UNDO, SAVE, DELETE
    /// </summary>
    private void DisplayConditionDatabase() {

      if (ConditionDB == null) {
        ConditionDB = ConditionDatabaseDAO.LoadConditionDB();
      }
      if (bufferedAbilitiesToSave == null || bufferedAbilitiesToSave.Count == 0)
        ReloadConditions();

      EditorGUILayout.BeginVertical("Box",GUILayout.Width(position.width - 10));
      scrollPosition = EditorGUILayout.BeginScrollView(scrollPosition);

      EditorGUILayout.BeginHorizontal();
      GUILayout.Label("Found " + bufferedAbilitiesToSave.Count + " condition(s) in the Database");
      GUILayout.FlexibleSpace();
      GUI.color = Color.white;
      if (GUILayout.Button("Create a new ConditionSO",GUILayout.Width(150))) {
        showNewCondition = true;
        ConditionToSave = new ConditionFactory();
      }
      if (GUILayout.Button("Save to Disk",GUILayout.Width(150))) {
        EditorUtility.SetDirty(ConditionDB);
        AssetDatabase.SaveAssets();
      }
      EditorGUILayout.EndHorizontal();
      GUILayout.Space(10);
      // ConditionSO Creation.
      if (showNewCondition) {

        EditorGUILayout.Foldout(true,ConditionToSave.Name);
        EditorGUILayout.BeginVertical("Box",GUILayout.Width(position.width - 20));
        ConditionToSave.id = ConditionDB.conditions.Count + 1;
        ConditionToSave = ConditionLayout(ConditionToSave);
        
        if (Utils.DisplayNewObjectButtons(ConditionToSave,ConditionDB.conditions,ref showNewCondition))
          ReloadConditions();
        EditorGUILayout.EndVertical();
      }

      for (int i = 0;i < bufferedAbilitiesToSave.Count;i++) {
        if (openedCondition.Count < bufferedAbilitiesToSave.Count)
          openedCondition.Add(false);

        openedCondition[i] = EditorGUILayout.Foldout(openedCondition[i],"" + bufferedAbilitiesToSave[i].Name);
        if (openedCondition[i]) {
          EditorGUILayout.BeginVertical("Box",GUILayout.Width(position.width - 20));
          ConditionLayout(bufferedAbilitiesToSave[i]);
          if (Utils.DisplayButtons(i,bufferedAbilitiesToSave,ConditionDB.conditions)) {
            openedCondition[i] = false;
          }
          EditorGUILayout.EndVertical();
        }
      }

      EditorGUILayout.EndScrollView();
      EditorGUILayout.EndVertical();
    }

    private ConditionFactory ConditionLayout(ConditionFactory c) {
      
      GUILayout.BeginVertical();

      c.targetFilter = (TargetFilter)EditorGUILayout.EnumPopup("Target Filter",c.targetFilter,GUILayout.Width(500));
      c.targetObject = (TargetObject)EditorGUILayout.EnumPopup("Target Object",c.targetObject,GUILayout.Width(500));

      c.operation = (Operator)EditorGUILayout.EnumPopup("Operation",c.operation,GUILayout.Width(500));
      
      //c.operation = EditorGUILayout.Popup("Operator",c.operation,ConditionSO.operators,GUILayout.Width(500));
      c.val = EditorGUILayout.TextField("Value",c.val,GUILayout.Width(500));

      c.modifier = (Modifier)EditorGUILayout.EnumPopup("Modifier",c.modifier,GUILayout.Width(500));
      // c.comparation = EditorGUILayout.Popup("Comparator",c.comparation,ConditionSO.comparator,GUILayout.Width(500));
      c.tag = EditorGUILayout.TextField("tags",c.tag,GUILayout.Width(500));

      GUILayout.EndVertical();
      return c;
    }

  }

}